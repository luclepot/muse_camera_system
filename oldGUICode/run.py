import cv2
from OLDcommands import paramChange, getInput, listify,linearParamChange
from OLDimage import Image
from OLDdata import Data
from OLDcamera import Camera
import numpy as np
import os 
import copy
import time
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
#import astropy.io
import sys
import platform
import ctypes
import winsound
from time import clock
import matplotlib
from scipy.optimize import curve_fit

class Run():
    def __init__(self,cameraType = "EO",runType = "Standard",
                 numberOfMeasurements = 20,averageRange =1,
                 expTime = 50,save=False,saveFile = "Temp",
                 saveName = "Untitled", view = True):
        self.saveFile = saveFile
        self.saveName = saveName

        precisionData = False
        targetCalibration = False
        cameraCalibration = False
        positional = False
        standard = False

        conversionType = "notSimple"
        if runType == "Precision":
            precisionData = True
            data = Data(NumberOfSamples = numberOfMeasurements,AverageRange = 1,camType = cameraType)
#            print data.F
#            print data.r0
#            print data.d0
#            print data.rPixel0
            alpha, gamma, threshold,expTime = data.parameters["camParams"]
            if not os.path.exists("files\{}".format(saveFile)):
                os.makedirs("files\{}".format(saveFile))
            divisor = 0.001
        elif runType == "Positional":
            positional = True
            data = Data(NumberOfSamples = numberOfMeasurements,AverageRange = averageRange,camType = cameraType)
            alpha, gamma, threshold,expTime = data.parameters["camParams"]
            if not os.path.exists("files\{}".format(saveFile)):
                os.makedirs("files\{}".format(saveFile))
            pFrameCount = averageRange + 1
                
        elif runType == "TargetCalibration":
            targetCalibration = True
            data = Data(NumberOfSamples = 50, AverageRange = 1, camType = cameraType , saveMetric = False)
            calibStart = False
            divisor = 0.001           
            
        elif runType == "CameraCalibration":
            cameraCalibration = True
            got = False
            while not got:
                try:
                    num = input("Please calibrate using a square chessboard pattern.\nEnter the number of internal corners in the chessboard calibration pattern: \n>>  ")
                    if num < 3:
                        raise UnboundLocalError
                    else:
                        got = True
                except:
                    print("please enter a number greater than 2")
            scale = 5
            cornerCount = 0
            criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
            objP = np.zeros((num*num,3),np.float32)
            objP[:,:2] = scale*np.mgrid[0:num,0:num].T.reshape(-1,2)
        
            objectPoints = []
            imagePoints = []
            conversionType = "simple"
            divisor = 0.001
        

        
        else:
            standard = True
            data = Data(NumberOfSamples = numberOfMeasurements,AverageRange = averageRange, camType = cameraType)
            alpha, gamma, threshold,expTime = data.parameters["camParams"]
            divisor = 0.001
        
        try:
            zero = np.load('files\zero.npz')
            self.zero = zero['arr_0']
        except IOError and KeyError:
            print "no previous zero loaded"
            self.zero = np.zeros((2,3))
        
        try:
            alpha += 0    
        except NameError:
            alpha = 0.28
            gamma = 0.019
            threshold = 103.3    
        self.selfdata = data
        cv2.destroyAllWindows()
        #cv2.namedWindow("Color")
        cv2.namedWindow("Edited")
        divisor = .0005
        camera = Camera(Type = cameraType,exposureTime=expTime)
        
        img = Image(camType = cameraType, calibrating = cameraCalibration)

        while 1:
            #t00 = clock()
            #t0 = clock()
            image = camera.grab()
            img.update(image)
            #print " image get time :", clock()-t0
            
            if img.got:
               # t0 = clock()
                #if not cameraCalibration:
                    #img.undistortImage()
                    #print "undistortion: ", clock() - t0
                #t0 = clock()
                img.convertTo8BitImage(Alpha = alpha, Gamma = gamma, Type = conversionType)
                #print "conversion: ", clock() - t0
                if not cameraCalibration:   
                    img.autoGray(thresh = threshold)
                    #t0 = clock()
                    if precisionData or standard or positional:
                        
                        data.addValues(img.fitEllipses(imageToDrawOn = img.color))    
                        
                        data.update(self.zero)
                        #print 'image process and update time', clock() - t0
                    elif targetCalibration and calibStart:
                        data.addValues(img.fitEllipses(imageToDrawOn = img.img))
                        data.update(self.zero)
                        data.sampleCount += 1
                        if data.sampleCount == data.numberOfSamples:
                            break
                        cnt = data.sampleCount
                    
                    #t0 = clock()
                    if precisionData:
                        if data.sampleCount < data.numberOfSamples: 
                            data.saveValues()
                        elif data.sampleCount == data.numberOfSamples:
                            if save:
                                self.standards = data.standard
                                self.metricSamples = data.metricSamples
                                self.sampleTimes = data.sampleTimes
                                self.ePropogation = data.ePropogation(data.metricSamples)
                                self.mean = np.mean(data.pixelsPerMM)
                                self.data = data
                            break
                        
                        cnt = data.sampleCount
                        #print 'p save time', clock() - t0
                    if positional:
                        if pFrameCount < averageRange:
                            pFrameCount += 1
                        if pFrameCount == averageRange:
                            data.saveValues()
                            pFrameCount += 1
                            winsound.PlaySound('SystemExit', winsound.SND_ALIAS)
                        cnt = pFrameCount
                    else:
                        cnt = data.sampleCount
                    #t0 = clock()    
                    img.img = cv2.bitwise_not(img.img)
                    img.addText(["alpha = {:.3f}".format(alpha),
                                 "gamma = {:.3f}".format(gamma),
                                 "pixels/mm = {:.3f}".format(np.mean(data.pixelsPerMM)),
                                 "sample count = {}".format(cnt)],
    #"outer/inner deviation = {:.1f}%".format(100.*(data.pixelPrime[0,3]/data.pixelPrime[1,3] - data.r0[0]/data.r0[1]))],
                                 img.img,size=0.60)
                    img.addText(["x pos = {:.3f}".format(np.mean(np.mean(data.metric,axis=0),axis=0)[0]),
                             "z pos = {:.3f}".format(np.mean(np.mean(data.metric,axis=0),axis=0)[1]),
                             "y pos = {:.3f}".format(np.mean(np.mean(data.metric,axis=0),axis=0)[2])],
                             img.img,size=0.60,height = 320)
                    #print "image addtext time: ", clock() - t0
                elif cameraCalibration:
                    img.addText(["{} corners found".format(cornerCount)],img.color,size=0.60)
            self.img = img  
            self.data = data
            self.camera = camera
            #t0 = clock()
            key = cv2.waitKey(1)
            if key == 27: 
                self.data = data
                break
            elif key == 32:
                if targetCalibration:
                    calibStart = True
                
                if positional:
                    winsound.PlaySound("*", winsound.SND_ALIAS)
                    pFrameCount = 0
                    print "Sample # ", data.sampleCount
                    
                if cameraCalibration:
                    ret, corners = cv2.findChessboardCorners(img.img, (num,num),None)
                    if ret == True:
                        objectPoints.append(objP)
                        cv2.cornerSubPix(img.img,corners,(11,11),(-1,-1),criteria)
                        imagePoints.append(corners)
                        img.img = cv2.drawChessboardCorners(img.img,(num,num),corners,ret)
                        cornerCount += 1
            elif key == 122:
                self.zero = np.mean(data.rawMetric,axis=0)
                np.savez('files\zero',self.zero)
                print "Zero set"
            elif key != -1 and key is not None:
                alpha, gamma, threshold = linearParamChange(divisor,alpha,gamma,threshold)
            #print "all key ops: ", clock()-t0
            #t0 = clock()
            cv2.circle(img.color,(640, 512),2,(2**16-1,2**16-1,2**16-1),1)
            #cv2.imshow("Color",cv2.resize(img.color,(0,0),fx=1.5,fy=1.5))
            cv2.imshow("Edited",cv2.resize(img.img,(0,0),fx=1.5,fy=1.5))
            #print 'show images: ', clock()-t0
            #print "Total time: ", clock()-t00
            
        camera.release()
        cv2.destroyAllWindows()   
        print np.shape(img.color)
        if cameraCalibration:
            if objectPoints and imagePoints and img.img is not None:
                ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objectPoints, imagePoints, img.img.shape[::-1],None,None)
                h,  w = img.img.shape[:2]
                cameraMatrix, roi = cv2.getOptimalNewCameraMatrix(mtx,dist,(w,h),0,(w,h))
                np.savez('files\{}CamParameters'.format(cameraType),a=roi,b=mtx,c=dist,d=cameraMatrix)
                print "parameters saved"
            else:
                print("could not save parameters, quitting")
        elif targetCalibration and np.abs(np.mean(data.pixel)) > 0:
            radii = np.zeros(2)
            print("please enter radii of circles in order from largest to smallest, in mm:")
            for i in range(np.size(radii)):
                radii[i] = getInput(inputPrompt = "radius {} = ".format(i+1),inputType = "raw")
            print("please enter distance of camera from target for calibration, in mm:")
            distance0 = getInput(inputPrompt = "distance = ")
            
            mean = np.mean(data.pixel,axis=0)
            variance = np.var(data.pixel,axis=0)
            print "\nsaving data...\n"
            
            np.savez('files\{}RingTarget{}'.format(data.c,cameraType),rWorld = radii,rPixel = listify(mean[:,3]),pixelVariances = listify(variance[:,3]),distance = distance0,camParams = [alpha,gamma,threshold,expTime])

            print "finished"
        
    def saveValues(self):
        self.data.hardSave(self.saveFile,self.saveName,self.metricSamples,self.sampleTimes,self.ePropogation,self.mean)
        print("data saved under 'files\{}\{}.npz".format(self.saveFile,self.saveName))
        
    def getHist(self,toHist,distance,Bins=12):
        font = {'family' : 'normal',
                'weight' : 'normal',
                'size'   : 22}
    
        matplotlib.rc('font', **font)
        raveled = np.ravel(toHist) - np.mean(np.ravel(toHist))
        numb = np.size(raveled)
        a = np.var(raveled)
        std = np.sqrt(a)
        A = ((np.max(raveled) - np.min(raveled))/Bins)/np.sqrt(2.*np.pi*a)*numb
        B = -0.5*(1.0/a)
        
        x = np.arange(-np.max(abs(raveled)),np.max(abs(raveled)),(np.max(raveled) - np.min(raveled))/np.size(raveled))
        def G(x,A,B):
            return A*np.exp(B*(x)**2)
        stdH = np.linspace(0,G(std,A,B),G(std,A,B)/2.)        
        
        self.fig = plt.figure()
        self.fig.suptitle("Binned X-Z plane Camera Measurements over {} Trials, {:.0f}mm distance".format(np.size(raveled),distance),fontsize=60)
        self.fig.set_figheight(20)
        self.fig.set_figwidth(30)        
        wid = 3.5
        col = '#b2bac4'
        plt.hist(raveled,normed=False,color='#041E42',bins=np.arange(np.min(raveled),np.max(raveled),(np.max(raveled) - np.min(raveled))/Bins))
        plt.plot(x,G(x,A,B),col,linewidth=wid)
        plt.plot(std*np.ones(np.size(stdH)),stdH,'#FFC72C',linewidth=wid)
        plt.plot(-std*np.ones(np.size(stdH)),stdH,'#FFC72C',linewidth=wid)
        plt.plot(np.zeros(np.size(stdH)),stdH*(G(0,A,B)/G(std,A,B)),'#63a56c',linewidth=wid)
        plt.xlabel("Deviation from XZ mean, mm",fontsize=45)
        plt.ylabel("Count",fontsize=45)
        plt.text(np.max(raveled)*.5,np.max(G(x,A,B)),"$\sigma$ = {:.4f}mm".format(std),fontsize=45)
        plt.plot([-std,.5*np.max(raveled)], [.75*G(-std,A,B),np.max(G(x,A,B))],'r--',linewidth=wid)
        plt.plot([std,.5*np.max( raveled)], [.75*G(-std,A,B),np.max(G(x,A,B))],'r--',linewidth=wid)
        plt.show()
        return self.fig
    
    def getLinearPlot(self,toPlot,x,stds,distance):
        font = {'family' : 'normal',
                'weight' : 'normal',
                'size'   : 35}
        matplotlib.rc('font', **font)
        raveled = np.ravel(toPlot) - toPlot[0]
        fig,ax = plt.subplots()
        fig.suptitle("Computer Calculated Position vs. World Position, X",fontsize=60)
        fig.set_figheight(20)
        fig.set_figwidth(20)  

        plt.title("Values averaged over {} Trials, {:.0f}mm distance".format(20,distance),fontsize=50)
        ax.xaxis.set_major_formatter(ticker.FormatStrFormatter('%0.3f'))
        (_, caps, _) = plt.errorbar(x,raveled,yerr=stds,xerr=.005,capsize=4,markersize=5,elinewidth=1.5,fmt='o')
        for cap in caps:
            cap.set_color("red")
            cap.set_markeredgewidth(1.5)
        #plt.plot(np.unique(x), np.poly1d(np.polyfit(x, raveled, 1))(np.unique(x)),color='#FFC72C')
        plt.plot(np.unique(x),np.unique(x),'--',color="black")
        plt.xlabel("World X-Position, mm (+/- 0.005 mm)",fontsize=45)
        plt.ylabel("Computer Calculated X-Position, mm",fontsize=45)
        #plt.text(np.min(x)*.85,np.max(x)*.85,"slope = {:.2f}".format(np.polyfit(x, raveled, 1)[0]),fontsize=45)
        plt.text(np.min(x)*.85,np.max(x)*.85,"average error = +/- {:.5f}mm".format(np.mean(stds)),fontsize=45)
         
