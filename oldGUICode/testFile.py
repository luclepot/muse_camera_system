import MUSEcameraSystem as MUSECS


main = False

if main:
    mainCam = MUSECS.CameraSystemObject()
    
    # do stuff
    
    i = 0
    mainCam.viewCameraFeed(viewType = mainCam.VIEW_8_BIT)
    while i < 2:
        mainCam.precisionMeasurement()
        i += 1
    ret = mainCam.getPrecisionPlotArrays()

    del mainCam
    
import sys
import glob
import serial


#def serial_ports():
#    """ Lists serial port names
#
#        :raises EnvironmentError:
#            On unsupported or unknown platforms
#        :returns:
#            A list of the serial ports available on the system
#    """
#    if sys.platform.startswith('win'):
#        ports = ['COM%s' % (i + 1) for i in range(256)]
#    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
#        # this excludes your current terminal "/dev/tty"
#        ports = glob.glob('/dev/tty[A-Za-z]*')
#    elif sys.platform.startswith('darwin'):
#        ports = glob.glob('/dev/tty.*')
#    else:
#        raise EnvironmentError('Unsupported platform')
#
#    result = []
#    for port in ports:
#        try:
#            s = serial.Serial(port)
#            s.close()
#            result.append(port)
#        except (OSError, serial.SerialException):
#            pass
#    return result

#ser = serial.Serial(
#        port='/dev/ttyUSB0',
#        baudrate=9600,
#        parity=serial.PARITY_NONE,
#        stopbits=serial.STOPBITS_TWO,
#        bytesize=serial.EIGHTBITS
#        )

ser = serial.Serial('COM4')
if not ser.isOpen():
    ser.open()
print"\nserial name -- ", ser.name
print"is open? -- ", ser.isOpen()
print ser.write(b'APPL 3.0, 1.0')

ser.close()
