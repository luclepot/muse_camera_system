from tkinter import *
from tkinter import ttk
import Tkinter as tk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure

class MainWindow(tk.Frame):
    def __init__(self, *args, **kwargs):
        tk.Frame.__init__(self, *args, **kwargs)
        #self.wm_geometry("%dx%d%+d%+d" % (self.winfo_width(),self.winfo_height(),25,25))
        self.aspect = [self.winfo_screenwidth(),self.winfo_screenheight()]
        self.grid()
        
        self.col1 = [tk.Label(self, text="Calibrations: ", font="Helvetica 12 bold"),
                     tk.Button(self, text="Calibrate Camera", command=self.CalibrateCameraWindow),
                     tk.Button(self, text="Calibrate Target", command=self.CalibrateTargetWindow)]
        self.col2 = [tk.Label(self, text="Samples: ", font="Helvetica 12 bold"),
                     tk.Button(self, text="View Raw Camera Feed", command=self.ViewCameraFeed),
                     tk.Button(self, text="Update Measurement", command=self.UpdateMeasurement)]
        self.buttonpack([self.col1,self.col2],80)
        
        fig = Figure()
        ax = fig.add_subplot(111)
        self.line, = ax.plot(range(10))

        self.canvas = FigureCanvasTkAgg(fig, master = self)
        self.canvas.show()
        self.canvas.get_tk_widget().pack(side='top', fill='both', expand=1)
        

    def buttonpack(self,butsets,n):
        wid = int(self.aspect[0]/len(butsets)/n)
        hgt = int(self.aspect[1]/len(butsets[0])/2/n)
        col = 0
        for butset in butsets:
            btn = 0
            for but in butset:
                but.grid(row=btn,column=col, padx = 0, pady=10)
                but.config(height = hgt, width = wid)
                if but.__class__.__name__ == "Button":
                    but.config(background="grey")
                btn+=1
            col += 1 
            
    def window(self,name):
        w = tk.Toplevel(self)
        w.wm_title(name)
        return w
    
    def CalibrateCameraWindow(self):
        try: self.ccw.destroy()
        except AttributeError: pass
    
        self.ccw = self.window("Camera Calibration")
        #self.ccw.wm_geometry("%dx%d%+d%+d" %)
        l = tk.Label(self.ccw, text="Camera Calibration Window")
        l.pack(side="top", fill="both", expand=True, padx=100, pady=100)
        
    def CalibrateTargetWindow(self):
        try: self.tcw.destroy()
        except AttributeError: pass
    
        self.tcw = self.window("Target Calibration")
        l = tk.Label(self.tcw, text="Target Calibration Window")
        l.pack(side="top", fill="both", expand=True, padx=100, pady=100)
    
    def ViewCameraFeed(self):
        try: self.rc.update()
        except AttributeError: pass

        self.rc = self.window("Camera Feed")
        l = tk.Label(self.rc, text="Camera Feed Window")
        l.pack(side="top", fill="both", expand=True, padx=100, pady=100)   
        
    def UpdateMeasurement(self):
        pass
            
if __name__ == "__main__":
    root = tk.Tk()
    main = MainWindow(root)
    main.pack(side="top", fill="both", expand=True)
    root.mainloop()
    