# The code for changing pages was derived from: http://stackoverflow.com/questions/7546050/switch-between-two-frames-in-tkinter
# License: http://creativecommons.org/licenses/by-sa/3.0/	

import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure

import Tkinter as tk
from Tkinter import *
import ttk


LARGE_FONT= ("Verdana Bold", 12)

class fakeCam:
    def calibrateCamera():
        pass
    def calibrateTarget():
        pass
    def precisionMeasurement():
        pass
    def getPrecisionPlotArrays():
        return []
    def getPrecisionPositionArrays():
        return []
    def flipBool():
        pass
    def getAxesLimits():
        return[]
    
mainCam = fakeCam()
    
def packbundle(*bundle):
    for item in bundle:
        item[0].grid(row=item[1],column=item[2],padx=30,pady=10)

class cameraGUI(tk.Tk):

    def __init__(self, *args, **kwargs):
        
        tk.Tk.__init__(self, *args, **kwargs)

        #tk.Tk.iconbitmap(self,"target.ico")
        tk.Tk.wm_title(self, "Position Monitor Controller")
        
        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand = True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}

        for F in (StartPage, PageOne, PageTwo, PageThree):

            frame = F(container, self)

            self.frames[F] = frame

            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(StartPage)

    def show_frame(self, cont):

        frame = self.frames[cont]
        frame.tkraise()

        
class StartPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self,parent)

        label = tk.Label(self, text="Home", font=LARGE_FONT)
        
        button = ttk.Button(self, text="Calibration",
                            command=lambda: controller.show_frame(PageOne))
    
        button2 = ttk.Button(self, text="Target View",
                            command=lambda: controller.show_frame(PageTwo))
    
        button3 = ttk.Button(self, text="Position View",
                            command=lambda: controller.show_frame(PageThree))
        
        for thing in [label, button, button2,button3]:
            thing.pack(padx=20,pady=15)
            
class PageOne(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Calibration", font=LARGE_FONT)
        label.pack(pady=10,padx=10)

        button1 = ttk.Button(self, text="Back",
                            command=lambda: controller.show_frame(StartPage))
        button1.pack()

        button2 = ttk.Button(self, text="Calibrate Camera"
                             , command = lambda: runobject.calibrate())
                            #,command=lambda: controller.show_frame(PageTwo))
        button2.pack()
        
        button3 = ttk.Button(self, text="Calibrate Target")
        button3.pack()

class PageTwo(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Target View", font=LARGE_FONT)
        label.pack(pady=10,padx=10)

        button1 = ttk.Button(self, text="Back",
                            command=lambda: controller.show_frame(StartPage))
        button2 = ttk.Button(self, text="Show Raw Camera Feed",
                            command=lambda: self.showTarget(mainCam.VIEW_RAW))
        button3 = ttk.Button(self, text="Show 8Bit Camera Feed",
                            command=lambda: self.showTarget(mainCam.VIEW_8_BIT))
        button4 = ttk.Button(self, text="Show Processed Camera Feed",
                            command=lambda: self.showTarget(mainCam.VIEW_PROCESSED))
        button1.pack()
        button2.pack()
        button3.pack()
        button4.pack()
        button1.pack()
        
        def showTarget(self, viewType):
            global mainCam
            global t
            def callback():
                mainCam.viewCameraFeed(viewType=viewType)
            try:
                if not t.isAlive():
                    t = threading.Thread(target=callback)
                    t.start()
                else:
                    print("Please wait for current function to finish before running another.")
            except AttributeError:
                t = threading.Thread(target=callback)
                t.start()
       # button2 = ttk.Button(self, text="")
                            #,command=lambda: controller.show_frame(PageOne))
        #button2.pack()

       # button2 = ttk.Button(self, text="")
                            #,command=lambda: controller.show_frame(PageOne))
        #button2.pack()


class PageThree(tk.Frame):
    
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Position View", font=LARGE_FONT)
        self.grid()
        button1 = ttk.Button(self, text="Back to Home",
                            command=lambda: controller.show_frame(StartPage))
        button2 = ttk.Button(self,text="Update",
                             command = lambda: update)
        #Ask "Are you sure you would like to update...?"
       
        v = StringVar()
        v.set("2")
        plabel = tk.Label(self,text="# of times in a row to update: ")
                          
        datelabel = tk.Label(self,text="Date: \t{}-\t{}".format(0,0))
        timelabel = tk.Label(self,text="Time: \t{}:\t{}".format(0,0))
        pinput = tk.Entry(self,text="frames")
        xlabel = tk.Label(self,text="Δ x: \t{} \t+/- {} mm".format(0,0))
        zlabel = tk.Label(self,text="Δ z: \t{} \t+/- {} mm".format(0,0))
        ylabel = tk.Label(self,text="Δ y: \t{} \t+/- {} mm".format(0,0))
        button3 = ttk.Button(self,text="Show/hide error bars", command = lambda : self.flipBool())
        setZero = ttk.Button(self, text="Set zero position", 
                             command = lambda : checksure("set zero position", self.setSystemZero))
        
        self.f = Figure(figsize=(12,12), dpi=60)
        self.xtime = self.f.add_subplot(311, xlabel="Time (hours)")
        self.ytime = self.f.add_subplot(312, xlabel="Time (hours)")
        self.ztime = self.f.add_subplot(313, xlabel="Time (hours)")
                
        
        packbundle([label,0,1],[button1,1,0],[button2,2,0],
                  [plabel,2,1],[pinput,2,2],[xlabel,4,0],[zlabel,5,0],
                  [ylabel,6,0],[datelabel,1,1],[timelabel,1,2],[button3,3,0],[setZero,3,2])

        self.canvas = FigureCanvasTkAgg(self.f, self)
        self.canvas.get_tk_widget().grid(row = 4, column = 1, columnspan = 2,rowspan = 4)
        #canvas.show()
        #canvas.show()

    def updatePlots(self):
        pass

#def update(self):
#==============================================================================
#         toolbar = NavigationToolbar2TkAgg(canvas, self)
#         toolbar.update()
#         canvas._tkcanvas.pack(row = 3, column = 1, columnspan = 2,rowspan = 3)
# 
#==============================================================================
    
app = cameraGUI()
app.mainloop()