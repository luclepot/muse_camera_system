# -*- coding: utf-8 -*-
"""
Document Info

File:
    MUSEGUImainConnected.py
Function:
    UM MUSE Group Target Position Monitor; Collects Height Data from and Excel Spreadsheet
Author:
    Sabrina Corsetti, University of Michigan Department of Physics
        sabcorse@umich.edu
Date Created:
    March 2018
Date Modified:
    April 2018
Version:
    Python 2.7, Spyder 3.2
"""

import pandas as pd

df = pd.read_excel('file_name_here.xlsx', sheet_name='Sheet1')
df.as_matrix()