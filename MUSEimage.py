"""
Function:
    UM MUSE Group Target Position Monitor, image processing subclass
"""
__author__ = "Luc Le Pottier, University of Michigan Department of Physics"
__email__ = "luclepot@umich.edu"

import cv2
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy as np
from MUSEclose import CloseCamera

class Img(CloseCamera):
    def process(self):
        '''

        Performs routine image processing functions - "convertTo8Bit",
        for example.
        '''
        self.convertTo8Bit()
        self.undistortImage()
        self.makeGray()
        self.fitEllipses()

    def convertTo8Bit(self):
        '''
        Map a 16-bit image through a lookup table to convert it to 8-bit, based
        on the initalized values for the lower and upper bounds
        (found in __init__)

        Modifies
        -------
        self.img

        '''
#
#        if (not(0 <= self.LB < 2**16) and self.LB is not None):
#            print('"lower_bound" must be in the range [0, 65535]')
#            self.LB = 0
#        if (not(0 <= self.UB < 2**16) and self.UB is not None):
#            print('"upper_bound" must be in the range [0, 65535]')
#            self.UB = 2**16
#        if self.LB is None:
#            self.LB = np.min(self.img16)
#        if self.UB is None:
#            self.UB = np.max(self.img16)
#        if self.LB > self.UB:
#            print('"lower_bound" must be smaller than "upper_bound"')
#            self.LB = self.UB
#
#        self.img = np.zeros_like(self.img16)
#        np.clip(self.img16, self.LB, self.UB, self.img)
#        self.img -= self.LB
#        self.img /= (65535/self.UB)
#        self.img = self.img.astype('uint8')

#        lut = np.concatenate(
#                [np.zeros(self.LB, dtype=np.uint16),
#                 np.linspace(0, 255, self.UB - self.LB).astype(np.uint16),
#                 np.ones(2**16 - self.UB, dtype=np.uint16)*255])
#        self.img = lut[self.img16.copy()].astype(np.uint8)
        self.img = cv2.convertScaleAbs(cv2.UMat(self.img16),
                                       alpha = 255.0/(self.UB - self.LB),
                                       beta = self.LB)

    def undistortImage(self):
        '''
        Uses the cv2 "undistort" function and perviously initalized camera calibration
        parameters to undistort image.

        '''
        self.img = cv2.undistort(
                self.img, self.B, self.C, None,
                self.D)

    def makeGray(self, sigma=0.33, scale=0.5):
        '''
        performs standard greying functions, including a bilateral  filter ..X

        '''
        # self.img = cv2.medianBlur(self.img, 9)
        # self.img = cv2.fastNlMeansDenoising(self.img)

        self.img = cv2.bilateralFilter(self.img, 9, 150, 150)
        ret, self.img = cv2.threshold(
                self.img, self.thresh, 255, cv2.THRESH_BINARY_INV)

    def fitEllipses(self, imageToDrawOn=None):
        '''
        This function is the meat of the image processing functions.
        Finds every contour of the image and calls the sub-function
        processContours() [detailed later] on each. Then converts the resultant
        list of fit ellipses, in pixel coordinates, to a matrix and saves it to
        'self.unprocessedPixelValues'.

        '''

#        self.oldImg = self.img
#        if self.pixelPrime.any():
#            x, y = np.mean(self.pixelPrime[:,0:2],axis=0)
#            MA = max(self.pixelPrime[:,4])
#            print "found: ", self.offset, " at: ", (x,y)
#            self.offset[0] = x
#            self.offset[1] = y
#
#            self.img = cv2.UMat(self.img, [int(y-MA),int(y+MA)], [int(x-MA ),int(x+MA)])
#
#        else:
#            print "not found: ", self.offset
#            self.offset.fill(0)



        # get contours the given image:

        self.contours, heirarchy = cv2.findContours(
                self.img,
                cv2.RETR_LIST,
                cv2.CHAIN_APPROX_NONE)

        # process found contours and put resultant pixel measurements into a
        # list in format [[x1, y1, ma1, MA1, angle1], [x2, y2, ma2, MA2, angle2], ....]
        values = [self.processContours(cnt) for cnt in self.contours]
        values = np.asarray(values) 
        # remove None type values
        values = filter(lambda x: x is not None, values)
        # try this:
        values = filter(lambda x: x[3] < 400, values)
        #filter out contours larger than the target by iterating through the contours
        #and removing any with a major axis larger than 300 oixels
        '''if len(values) != 0:
            for i in range(len(values)):
                print (len(values))
                print(i)
                print(values[i])
                if values[i][3] > 300:
                    values.pop(i)'''
                
        # save to an array
        self.unprocessedPixelValues = np.asarray(values)
        '''
        #PRoy: Feb 2019***************
        print("\n")
        print("values = ", np.asarray(values))
        print("values.shape = ", np.asarray(values).shape)
        print("\n")
        #PRoy: Feb 2019 **************'''

    def sizableArea(self, cnt, minArea=2500):
        '''
        REQUIRED:
            input cnt, a cv2 style contour
        OPTIONAL:
            minArea, the minimum area for the contour. Initally set to 7500

        Returns true if the contour is greater than the minimum area

        '''
        if cv2.contourArea(cnt) > minArea:
            return True
        else:
            return False

    def notFourSided(self, cnt):
        '''
        REQUIRED:
            input cnt, a cv2 style contour

        Returns true if the contour is not a quadralateral

        '''
        peri = cv2.arcLength(cnt, True)
        approx = cv2.approxPolyDP(cnt, 0.02 * peri, True)
        return not len(approx) == 4

    def isCircle(self, cnt):
        '''
        REQUIRED:
            input cnt, a cv2 style contour

        Returns true if the contour is less than self.mCVFR % deviation from a
        perfect circle.

        '''
        (x, y), r = cv2.minEnclosingCircle(cnt)
        rx2 = [i**2. for i in (cnt[:, :, 0] - x)]
        ry2 = [i**2. for i in (cnt[:, :, 1] - y)]
        rprime = np.asarray([np.sqrt(i + j) for i, j in zip(rx2, ry2)])
        return True if (abs(1-np.mean(rprime*(1./r))) < self.mCVFR) else False

    def processContours(self, cnt):
        '''
        REQUIRED:
            input cnt, a cv2 style contour

        Performs a series of tests on contour cnt. If they pass, fits an
        ellipse to the contour and extracts the position, major and minor axes,
        and angle of skew, in pixels.
        Returns these values in list format.

        '''
        if self.sizableArea(cnt):
            if self.notFourSided(cnt):
                if self.isCircle(cnt):
                    ellipse = cv2.fitEllipse(cnt)
                    (x, y), (ma, MA), angle = ellipse
                    value = [x, y, ma, MA, angle]
                    # cv2.ellipse(imageToDrawOn,ellipse,(0,0,self.bits),1)
                    # cv2.circle(imageToDrawOn,(int(round(x)),int(round(y))),
                    # 1,(0,0,self.bits),1)
                    return value

    def histit(self, img):
        '''
        Creates a histogram of the image's pixel hue,
        putting each pixel hue into a bin for the image.
        RETURNS:
            img, image containing written histogram.

        '''
        fig = Figure()
        canvas = FigureCanvas(fig)
        sp = fig.add_subplot(111)
        sp.set_xlim([0, self.bitnum])
        sp.set_ylim([0, np.size(img)])
        sp.hist(img.reshape((1, np.size(img)))[0], bins=14)
        width, height = fig.get_size_inches() * fig.get_dpi()
        canvas.draw()       # draw the canvas, cache the renderer
        img = np.fromstring(
                canvas.tostring_rgb(),
                dtype='uint8').reshape(int(height), int(width), 3)
        # ax.plt.hist(self.img.reshape((1,np.size(self.img)))[0])
        # canvas.draw()
        # image = np.fromstring(canvas.tostring_rgb(),dtype='uint8')
        return img
