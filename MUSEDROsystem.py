# -*- coding: utf-8 -*-
"""
Function:
    UM MUSE Group DRO readout automation through pywinauto
"""
__author__ = "Matthew Dimond, University of Michigan Department of Physics"
__email__ = "mtdimon@umich.edu"

from pywinauto.application import Application
from pywinauto.controls.win32_controls import ButtonWrapper
from pywinauto.controls.win32_controls import ComboBoxWrapper
#import time
from time import sleep
import os
from pandas import DataFrame, read_csv
import pandas as pd 


## start the process
##debug
##set the buttons that we are going to automate.
class DROSystemObject():
    measurement_count = 0
    
    """function to initialize variables used by pywinauto, later need to implement 
    a check to make sure DRO is connectd properly"""
    def __init__(self, filename):
        self.utility = Application(backend="uia").start(r"C:\Program Files (x86)\EL Series utility.exe")
        self.sheet = filename
        self.openDROFile()    
        
    """opens a file that the DRO writes to"""
    def openDROFile(self):
        self.utility['EL Series software'].Button5.click()
        saveAs = self.utility['EL Series software']['Save As']
        saveAs.filename2.type_keys(
                'C:\Users\musecamerasystem\Documents\muse_camera_system\DROreadings\\' + 
                self.sheet)
        #sleep(2)
        saveAs.type_keys('{ENTER}')
        #sleep(2)
        
    """ clicks the "curr val" button, and indexes the current row up one"""
    def TakeDROReading(self):
        curr_val = self.utility['EL Series software']['Button 6']
        curr_val.click()
        self.measurement_count += 1
        
    """function to read in file given by filename, and read a row. currently only returns x, y, z
    positions in [x_cal, y_val, z_val] array"""
    
    def ReadHeightData(self, row):
        data_sheet = pd.read_excel('C:\Users\musecamerasystem\Documents\muse_camera_system\DROreadings\\' + self.sheet)
        xyz = [data_sheet['X'][row], data_sheet['Y'][row], data_sheet['Z'][row]]
        return xyz
    

    '''ideally closes the DRO, clears the spreadsheet'''
    def EndAutoDRO(self):
        self.utility['EL Series software'].send_keys("%{F4}")
    ''' 
    """destructor"""
         def __del__(self):
        self.EndAutoDRO()
    '''






