"""
Function:
    UM MUSE Group Target Position Monitor, data handling and processing subclass
"""
__author__ = "Luc Le Pottier, University of Michigan Department of Physics"
__email__ = "luclepot@umich.edu"

import numpy as np
import copy
import time
import traceback
import matplotlib.pyplot as plt
import matplotlib.colors as pltc
#import MUSEDROsystem as mds
from MUSEclose import CloseCamera
import os
import pandas as pd
from datetime import datetime
import pickle
class Data(CloseCamera):

    def convertUnits(self, var, i, vert_dist):
        '''
        x : ellipse's center pixel number in x direction
        y : ellipse's center pixel number in y direction
        ma: minor axis dia.in pixels
        MA: major axis dia. in pixels
        vert_dist: vertical distance between bull's eye & lens using DRO &
        known dimensions of ladder, pump system etc.
        '''
        
        x, y, ma, MA, ang = var
        r = MA 

        #######################################
        #Vertical distance measured by camera
        #######################################
        '''
        To determine vertical distance between bull's eye & lens:
            
        pixel_per_mm at this distance = (MA/2)/self.r0, where 
        r0[i] are user input values of the known radii of bull's eye's circles. 
        We know pixel_per_mm = pow(vertical_distance, -1.06)*5229.5 from lab study. 
        Use this to back-calculate vertical distance from measure pixel_per_mm
        '''
        
        d = ((2 * 2964.1 * self.r0[i]) / MA) ** (1/0.97) 
        
        #####################################
        #Center of bull's eye in mm
        ######################################
        '''
        We know pixel_per_mm = pow(vertical_distance, -1.06)*5229.5 from lab study.
        X (in mm) = x/pixel_per_mm(vertical_distance)
        '''
        
        #Determine X, Y of circle's center in mm
        xm, ym = [self.pixelsToMM(x, vert_dist), self.pixelsToMM(y, vert_dist)]
        return [xm, ym, d]
    
    def pixelsToMM(self, pixels, d):
        #pixels/mm function featuring empirically derived constants.
        
        pixels_per_mm = 2964.1  * (d ** (-0.97 ))
        mm_per_pixel = 1.0 / pixels_per_mm
        
        return (pixels * mm_per_pixel)
    
    def find_peaks(self, a):
      x = np.array(a)
      max = np.max(x)
      length = len(a)
      ret = []
      for i in range(length):
          ispeak = True
          if i-1 > 0:
              ispeak &= (x[i] > 1.8 * x[i-1])
          if i+1 < length:
              ispeak &= (x[i] > 1.8 * x[i+1])

          ispeak &= (x[i] > 0.05 * max)
          if ispeak:
              ret.append(i)
      return ret

    def cutFields(self, fields, cuts):
        for i, cut in enumerate(cuts):
            if cut[0] is not None:
                for f in fields:
                    if f is not fields[i]:
                        f[fields[i] < cut[0]] = float('inf')
                fields[i][fields[i] < cut[0]] = float('inf')
            if cut[1] is not None:
                for f in fields:
                    if f is not fields[i]:
                        f[fields[i] > cut[1]] = float('inf')
                fields[i][fields[i] > cut[1]] = float('inf')
        return fields

    def getVoltage(self, dist):
        if not self.voltageCalibrated or self.voltageParams[1] == self.d0:
            return 23.0
        else:
            m = ((23.0 - self.voltageParams[0]) / (self.d0 - self.voltageParams[1]))
            return m*dist + 23.0 - m*self.d0

    def refineCalibrationInfo(self, field, ffield, tfield, UBrange, Trange,
                               cuts = tuple(((None, None), (None, None), (None, None)))):

        ##(ubi, ti) = np.unravel_index(self.field.argmax(), self.field.shape)
        data = np.load("GUIfiles/gridSearchData.npz")
        ofield = data["f"]
        otfield = data["tf"]
        offield = data["ff"]

        field, ffield, tfield = self.cutFields(tuple((ofield, offield, otfield)), cuts)


        ## cost matrix
        #        nfield = np.sqrt(
        #                (field/np.max(field[~np.isinf(field)]))**2. +
        #                (tfield/np.max(tfield[~np.isinf(tfield)]))**2. +
        #                (ffield/np.max(ffield[~np.isinf(ffield)]))**2.
        #                )

        #binsp = UBrange.size*Trange.size/10.0
        #if binsp < 4:
        #    binsp = 4

        #binsp = int(binsp)
        #plt.hist(nfield[~np.isinf(nfield)].ravel(), bins=binsp)
        #plt.show()

        labels = ["sample time", "diff in real/meas. axis ratio", "diff in ellipse axes"]

        ## selects the top p% of values in every field
        found = 0
        p = 0.05
        foundn = 10
        if foundn > self.UBrange.size*self.Trange.size/3:
            foundn = self.UBrange.size*self.Trange.size/3
        while found < foundn:
            k = int(p*field.size)
            p += 0.005
            ffidx = np.unravel_index(np.argpartition(ffield.ravel(), k)[:k], ffield.shape)
            tfidx = np.unravel_index(np.argpartition(tfield.ravel(), k)[:k], tfield.shape)
            fidx = np.unravel_index(np.argpartition(field.ravel(), k)[:k], field.shape)
            bestIndex = tuple(np.array(list(set(map(tuple, np.array(ffidx).transpose())) &
                                            set(map(tuple, np.array(    fidx).transpose())) &
                                            set(map(tuple, np.array(tfidx).transpose())))).transpose())
            if len(bestIndex) > 0:
                found = len(bestIndex[0])

        toPlotGood = tuple((tfield[bestIndex], field[bestIndex], ffield[bestIndex]))
        ffidxp = np.unravel_index(np.argpartition(ffield.ravel(), k)[k:], ffield.shape)
        tfidxp = np.unravel_index(np.argpartition(tfield.ravel(), k)[k:], tfield.shape)
        fidxp = np.unravel_index(np.argpartition(field.ravel(), k)[k:], field.shape)
        worstIndex = tuple(np.array(list(set(map(tuple, np.array(ffidxp).transpose())) |
                set(map(tuple, np.array(    fidxp).transpose())) |
                set(map(tuple, np.array(tfidxp).transpose())))).transpose())
        toPlotBad = tuple((tfield[worstIndex][tfield[worstIndex] < float('inf')],
            field[worstIndex][tfield[worstIndex] < float('inf')],
            ffield[worstIndex][tfield[worstIndex] < float('inf')]))
        toPlotBadOld = tuple((otfield[worstIndex][otfield[worstIndex] < float('inf')],
            ofield[worstIndex][otfield[worstIndex] < float('inf')],
            offield[worstIndex][otfield[worstIndex] < float('inf')]))
        plt.rcParams["figure.figsize"] = (18, 24)
        for side in range(3):
            plt.subplot(3,2,1 + 2*side)
            plt.title(labels[side % 3] + " vs. " + labels[(side + 1) % 3] + ", CUT:")
            plt.plot(toPlotBad[side % 3], toPlotBad[(side + 1) % 3], 'bo')
            plt.plot(toPlotGood[side % 3], toPlotGood[(side + 1) % 3], 'ro')
            plt.xlabel(labels[side % 3], fontsize=20)
            plt.ylabel(labels[(side + 1) % 3], fontsize=20)
            plt.xticks(fontsize=15)
            plt.yticks(fontsize=15)
            

            plt.subplot(3,2,2 + 2*side)
            plt.title(labels[side % 3] + " vs. " + labels[(side + 1) % 3] + ", UNCUT:")
            plt.plot(toPlotBadOld[side % 3], toPlotBadOld[(side + 1) % 3], 'bo')
            plt.plot(toPlotGood[side % 3], toPlotGood[(side + 1) % 3], 'ro')
            plt.xlabel(labels[side % 3],fontsize=20)
            plt.ylabel(labels[(side + 1) % 3],fontsize=20)
            plt.xticks(fontsize=15)
            plt.yticks(fontsize=15)

        plt.show()
        self.bestIndex = bestIndex


    def removeCopiesOld(self):
        if self.pixelOversized.shape[0] < 2:
            return False
        elif self.pixelOversized.shape[0] == 2:
            self.pixelPrime = self.pixelOversized
            return True

        self.pixelPrime.fill(0)
        self.pixelOversized = self.pixelOversized[
                np.argsort(self.pixelOversized[:, 3])[::-1]]
#        for i in range(self.pixelOversized.size(axis=0)):
#            if
#
        copied = copy.deepcopy(self.pixelOversized)
        self.c = 0
        while np.size(copied) > 0:
            index = np.where(
                    abs((copied[0, 3] - copied[:, 3])/copied[0, 3]) <
                    self.maxPercentSimilarityOfFoundCircles*0.01)
            copied2 = copied[index]
            if np.size(copied2) > 0:
                self.pixelPrime[self.c] = np.mean(copied2, axis=0)
                copied = np.delete(copied, index, axis=0)
                self.c += 1
            else:
                break
        return True

    def removeCopies(self):
        """
        removes copies and returns True if valid target from pixelOversized, False if invalid.
        """
        if self.pixelOversized.shape[0] < 2:
            if self.pixelOversized.shape[0] == 1:
                self.pixelPrime = self.pixelOversized
                self.pixelPrime = np.append(self.pixelPrime, self.pixelOversized, axis = 0)
                return True;
            return False
        elif self.pixelOversized.shape == (2, 5):
            
            return True
        #sort pixel oversized
        self.pixelOversized = self.pixelOversized[
                np.argsort(self.pixelOversized[:, 3])]

        self.pixelPrime.fill(0) 
        #why is this a global to the class
        self.c = 0
        i = 0
        l = 1
        #loop through all of the fitted ellipses in pixeloversized
        while i < self.pixelOversized.shape[0] and self.c < 2:
            value = self.pixelOversized[i, 3] #value is the major axis of every ellipse
            #i do think this just removes copies based on some bizarro MA calculation
            if (abs(value - np.mean(self.pixelOversized[(i + l)%self.pixelOversized.shape[0], 3]))/abs(value)) > self.maxPercentSimilarityOfFoundCircles*0.01:
                
                self.pixelPrime[self.c] = np.mean(self.pixelOversized[i:(i+l)], axis=0)
              
                self.c += 1
                i = l
                l = 1
            else:
                l += 1
        return True
    
    
    
    '''checks the "pixelPrime" array for length- this is the number of circles seen.
    if it is of length 1 (ie, if the inner circle is the only one available), copies that ellipse to fit.
    NOTE: this is sorta a hacky fix- I don't know how this vector interacts with everything else. This potentially affects the error calculation negatively-
    the standard error will be higher than it should be. Also, add in a check for the inner circle diameter so that we aren't randomly adding in bad circles.
    def checkNumCircles(self):
        
            self.pixelPrime.append(self.pixelPrime[0])
            """debug"""
            print("Only found once Ellipse, copying")
            for ellipse in self.pixelPrime:
                print(ellipse)
                
        if self.pixelPrime.shape[0] == 2:
            return True
        return False
    
    '''
    def getInput(self, inputType = 1, inputPrompt="input: "):
        while True:
            try:
                if type(inputType) == int or type(inputType) == float:
                    var = input(inputPrompt)
                    break
                if type(inputType) == str:
                    var = raw_input(inputPrompt)
                    break
            except ValueError or NameError:
                print("       error, please re-enter value or check that value is of correct type")
        return var



    def listify(self, Set):
        return [Set[i] for i in range(Set.size)]

    def addValues(self, pM_SizedPixelData):
        try:
            self.pixelOversized = pM_SizedPixelData
        except Exception:
            print "       input array size does not match initial array size"

# P Roy: 27 Apr 2019:  Added DRO's vertical distance as argument to update func.
    def update(self, vertical_dist):
        f = open('pixels.txt', 'a+')
        try:
            if (self.removeCopies()):
               
                if self.saveMetric:
                    for i in range(2):
                        #PRoy: 27 Apr 2019, added vertical_dist
                        self.metricPrime[i, :] = np.asarray(
                                self.convertUnits(self.pixelPrime[i, :], i, vertical_dist))
                
                
                self.pixel[0] = self.pixelPrime
                
                outer = str(self.pixelPrime[0])
                center_x_outer = np.around(self.pixelPrime[0][0], 2)
                outer_major_ax = np.around(self.pixelPrime[0][2], 2)
                outer_minor_ax = np.around(self.pixelPrime[0][3],2)
                
                inner = str(self.pixelPrime[1])
                center_x_inner = np.around(self.pixelPrime[1][0],2)
                inner_major_ax = np.around(self.pixelPrime[1][2],2)
                inner_minor_ax = np.around(self.pixelPrime[1][3],2)
                outer = outer.replace("[", "")
                outer = outer.replace("]", "")
                inner = inner.replace("[", "")
                inner = inner.replace("]","")                        
                f.write(outer)
                f.write(" ")
                f.write(inner)
                f.write("\n")
            
                print("\n")
                print("outer (center pxl's x, ma, MA):")
                print(center_x_outer, outer_major_ax, outer_minor_ax)
                print("\n")
                print("inner (center pxl's x, ma, MA):")
                print(center_x_inner, inner_major_ax, inner_minor_ax)
                print("\n")
                
                self.pixel = np.roll(self.pixel, 1, axis=0)
                self.rawMetric[0] = self.metricPrime
                self.metric[0] = self.metricPrime
                self.metric = np.roll(self.metric, 1, axis=0)
                
                #Roy, Apr 27, 2019
                #print("This is update function")
                #print("PixelPrime: ", self.pixelPrime)
                #print("metric = metricPrime:", self.metricPrime)
                
                self.rawMetric = np.roll(self.rawMetric, 1, axis=0)
                self.updated = True
            else:
                self.updated = False
                pass
        except:
            print traceback.format_exc()
            self.updated = False
            pass
        f.close()
#    def ePropogation(self, var):
#        return [np.sqrt(x) for x in np.sum([
#                [j*j for j in np.std(var, axis=0)[:,
#                 i]] for i in range(np.size(np.std(var, axis=0)[0, :, 0]))],
#            axis=1)]

    def ePropogation(self, var):
        return np.std(var[:,0], axis=0)
#        return [np.sqrt(x) for x in np.sum([
#                [j*j for j in np.std(var, axis=0)[:,
#                 i]] for i in range(np.size(np.std(var, axis=0)[0, :, 0]))],
#            axis=1)]

    def hardSave(self, setName="temp", saveName="tempData", *args):
        np.savez("GUIfiles\{}\{}.npz".format(setName, saveName), *args)

    def saveValues(self):
        try:
            if np.mean(self.metric) != 0.0000:
                #save the pixelvalues in the metricsamples array
                self.metricSamples[self.sampleCount] = np.mean(
                        self.metric, axis=0)
                
                #Roy, Apr 27, 2019
        
                #print("This is saveValues function")
                #print("self.metricSamples[self.sampleCount] = ", self.metricSamples)
                
#                self.standard[self.sampleCount] = self.ePropogation(
#                        self.metric)
                self.sampleTimes[self.sampleCount] = time.time()
                self.sampleCount += 1
                if self.sampleCount == self.numberOfSamples:
                    self.precision = self.ePropogation(self.metricSamples)
                    self.precisionMeasurements[
                            self.precisionMeasurementCount] = np.asarray(
                            [np.mean(self.metricSamples, axis=0)[0],
                             self.precision])
                    self.precisionMeasurementTimes[
                            self.precisionMeasurementCount] = time.time()
                    self.precisionMeasurementCount += 1

        except Exception:
            exception = traceback.format_exc()
            print "sample couldn't be saved"
            print exception

    def resetData(self):
        self.precisionMeasurementCount = 0
 
    def getPrecisionPlotArrays(self, zero=True):
        '''
        Returns a numpy array where the rows are as follows,
        with the columns as sample #:
        [ t values (meas. time - first meas. time, days ]
        [   x values (x-position of measurement, mm)    ]
        [   z values (z-position of measurement, mm)    ]
        [   y values (y-position of measurement, mm)    ]

        '''
        i = self.precisionMeasurementCount
        v = self.precisionMeasurements
        
        #Roy, Apr 27. 2019
        #print("This is getPrecisionPlotArrays")
        #print("i = ", i)
        #print("shape of v = ",v.shape)
        #for j in range(i):
         #   print("v[0:i, 0, :, 0], axis=1)[j]:", v[0:10, 0, :, 0][j])
         
         
        if zero: z = self.zero
        else: z = [0, 0, 0]
        t = np.asarray(
                [(self.precisionMeasurementTimes[
                        j] - self.precisionMeasurementTimes[
                                0]) for j in range(
                        self.precisionMeasurementCount)])

        return np.asarray(
                [t,
                 np.asarray([np.mean(v[0:i, 0, :, 0], axis=1)[j] - z[0] for j in range(i)]),
                 np.asarray([np.mean(v[0:i, 0, :, 1], axis=1)[j] - z[1] for j in range(i)]),
                 np.asarray([np.mean(v[0:i, 0, :, 2], axis=1)[j] - z[2] for j in range(i)])
                 ,])
    
    def getPrecisionPositionArrays(self, zero=True):
        '''
        Returns a numpy array of the following type:
        [  x value (x-position of measurement, mm), x value precision (mm)  ]
        [  z value (z-position of measurement, mm), z value precision (mm)  ]
        [  y value (y-position of measurement, mm), y value precision (mm)  ]

        '''
        i = self.precisionMeasurementCount
        considered = self.precisionMeasurements[i-1]
        if zero: z = self.zero
        else: z = [0,0,0]
        positions =  np.asarray([np.mean(considered[0], axis=0)[j] - z[j] for j in range(3)])
        #takes the variance from the stdev measurements, sums them, divides by samplenumber? and takes the square root. (somehow a factor of 10 s brinh )
        #stds = np.asarray([np.sqrt(np.sum([considered[1,h,j]**2 for h in range(2)])/self.numberOfSamples) for j in range(3)])
        
        #Roy, 27 Apr 2019
        #print("This is getPrecisionPositionArrays")
        #print("considered[0][0] = ", considered[0][0])
        #print("considered[0][1] = ", considered[0][1])
        
        '''
        Roy, 27 Apr 2019:
        considered[0][0]: [x_mean, y_mean,z_mean] in mm for circle1 of bull's eye
        considered[0][1]: [x_mean, y_mean,z_mean] in mm for circle2 of bull's eye
        '''
        #Find average of standard error of the mean of the center 
        #pixel position of each circle. 
        #There are 2 circles, so there are 2 standard errors. Average them.
        
        stds = np.asarray([np.sqrt(np.sum([considered[1,h,j]**2 for h in range(2)])/2) for j in range(3)])
        sterrs = np.zeros_like(stds)
         
        for j in range(len(stds)):
                sterr = stds[j]/np.sqrt(self.numberOfSamples)
                sterrs[j] = sterr
      
        #P Roy, 27 Apr 2019:
        #To the average standard error, add 0.5*(difference in mean of center
        #positions of the two circles). It is related to contour fit's error. 
        
        delta_means = abs(considered[0][0]-considered[0][1])*0.5
        sterrs = sterrs + delta_means
        #print("delta_means = ", delta_means)
        #print("sterrs = ", sterrs)
                
        return np.asarray([positions, sterrs])
    
    # self.precisionMeasurements[0:self.precisionMeasurementCount]

    def getErrors(self):
        i = self.precisionMeasurementCount
        considered = self.precisionMeasurements[0:i]    
        #takes the variance from the stdev measurements, sums them, divides by samplenumber? and takes the square root.
        stds = np.asarray([[np.sqrt(np.sum([considered[k,1,h,j]**2 for h in range(2)])/2) for j in range(3)] for k in range(i)])
    
        #convert standard deviations to standard error
        sterrs = np.zeros_like(stds)
        delta_means = np.zeros_like(stds)
        for i in range(len(stds)):    
            for j in range(len(stds[i])):
                sterr = stds[i][j]/np.sqrt(self.numberOfSamples)
                sterrs[i][j] = sterr
                #P Roy, 27 Apr 2019:
                #To the average standard error, add 0.5*(difference in mean of center
                #positions of the two circles). It is related to contour fit's error.
                delta_means[i][j] = abs(considered[i][0][0][j]-considered[i][0][1][j])*0.5 
                sterrs[i][j] = sterrs[i][j] + delta_means[i][j]
        '''
        print("Thi is getErrors function")
        print("sterrs = ", sterrs)
        print("delta means = ", delta_means)
        '''
        
        return sterrs

    def getCSVArray(self, zero = True):
        '''
        Returns a numpy array of the following type:
        [t values (meas. time - first meas. time, days ]
        [  x value (x-position of measurement, mm)]
        [  x value precision (mm)  ]
        [  z value (z-position of measurement, mm)]
        [  z value precision (mm)  ]
        [  y value (y-position of measurement, mm)]
        [  y value precision (mm)  ]

        '''
        i = self.precisionMeasurementCount
        v = self.precisionMeasurements
        if zero: z = self.zero
        else: z = [0, 0, 0]
        t = np.asarray(
                [(self.precisionMeasurementTimes[
                        j] - self.precisionMeasurementTimes[
                                0]) for j in range(
                        self.precisionMeasurementCount)])
        errors = self.getErrors()
        return np.asarray(
                [t,
                 np.asarray([np.mean(v[0:i, 0, :, 0], axis=1)[j] - z[0] for j in range(i)]),
                 np.asarray([errors[j][0] for j in range(i)]),
                 np.asarray([np.mean(v[0:i, 0, :, 1], axis=1)[j] - z[1] for j in range(i)]),
                 np.asarray([errors[j][1] for j in range(i)]),
                 np.asarray([np.mean(v[0:i, 0, :, 2], axis=1)[j] - z[2] for j in range(i)]),
                 np.asarray([errors[j][2] for j in range(i)])
                 ,])
        return pplot
       