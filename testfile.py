
#import cv2
#import numpy as np 
#import traceback

#from MUSEpowerControl import PowerSupply
#global p
#p = PowerSupply()
#p.connect('GPIB0::1::INSTR')
#p.setVolt(18)
#p.connect('GPIB0::1::INSTR')

##(ubi, ti) = np.unravel_index(self.field.argmax(), self.field.shape)

def cutFields(fields, cuts):
    for i, cut in enumerate(cuts): 
        if cut[0] is not None:
            for f in fields:
                if f is not fields[i]:
                    f[fields[i] < cut[0]] = float('inf')
            fields[i][fields[i] < cut[0]] = float('inf')
        if cut[1] is not None:
            for f in fields:
                if f is not fields[i]:
                    f[fields[i] > cut[1]] = float('inf')
            fields[i][fields[i] > cut[1]] = float('inf')
    return fields

data = np.load("GUIfiles/gridSearchData.npz")
ofield = data["f"]
otfield = data["tf"]
offield = data["ff"]

tfield[np.isnan(field)] = float('inf')
field[np.isnan(field)] = float('inf')
field[tfield < 0] = float('inf')
tfield[tfield < 0] = float('inf')
field, ffield, tfield = cutFields(tuple((field, ffield, tfield)), tuple(((1.22, 1.267), (0, .024), (0, 0.065))))


## cost matrix
#        nfield = np.sqrt(
#                (field/np.max(field[~np.isinf(field)]))**2. + 
#                (tfield/np.max(tfield[~np.isinf(tfield)]))**2. + 
#                (ffield/np.max(ffield[~np.isinf(ffield)]))**2.
#                )

#binsp = UBrange.size*Trange.size/10.0
#if binsp < 4:
#    binsp = 4
 
#binsp = int(binsp)
#plt.hist(nfield[~np.isinf(nfield)].ravel(), bins=binsp)
#plt.show()

labels = ["sample time", "diff in circles' axis ratio", "diff in ellipse axes"]         

## selects the top p% of values in every field
found = 0
p = 0.05
while found < 10:
    k = int(p*field.size)
    p += 0.005
    ffidx = np.unravel_index(np.argpartition(ffield.ravel(), k)[:k], ffield.shape)
    tfidx = np.unravel_index(np.argpartition(tfield.ravel(), k)[:k], tfield.shape)
    fidx = np.unravel_index(np.argpartition(field.ravel(), k)[:k], field.shape)
    bestIndex = tuple(np.array(list(set(map(tuple, np.array(ffidx).transpose())) & 
                                    set(map(tuple, np.array(    fidx).transpose())) &
                                    set(map(tuple, np.array(tfidx).transpose())))).transpose())
    if len(bestIndex) > 0:
        found = len(bestIndex[0])

toPlotGood = tuple((tfield[bestIndex], field[bestIndex], ffield[bestIndex]))
ffidxp = np.unravel_index(np.argpartition(ffield.ravel(), k)[k:], ffield.shape)
tfidxp = np.unravel_index(np.argpartition(tfield.ravel(), k)[k:], tfield.shape)
fidxp = np.unravel_index(np.argpartition(field.ravel(), k)[k:], field.shape)
worstIndex = tuple(np.array(list(set(map(tuple, np.array(ffidxp).transpose())) |
        set(map(tuple, np.array(    fidxp).transpose())) | 
        set(map(tuple, np.array(tfidxp).transpose())))).transpose())    
toPlotBad = tuple((tfield[worstIndex][tfield[worstIndex] < float('inf')], 
    field[worstIndex][tfield[worstIndex] < float('inf')], 
    ffield[worstIndex][tfield[worstIndex] < float('inf')]))        
toPlotBadOld = tuple((otfield[worstIndex][otfield[worstIndex] < float('inf')], 
    ofield[worstIndex][otfield[worstIndex] < float('inf')], 
    offield[worstIndex][otfield[worstIndex] < float('inf')])) 
for side in range(3):
    plt.subplot(1,2,1)
    plt.title(labels[side % 3] + " vs. " + labels[(side + 1) % 3] + ", CUT:")
    plt.plot(toPlotBad[side % 3], toPlotBad[(side + 1) % 3], 'bo')
    plt.plot(toPlotGood[side % 3], toPlotGood[(side + 1) % 3], 'ro')
    plt.xlabel(labels[side % 3])
    plt.ylabel(labels[(side + 1) % 3])
    
    plt.subplot(1,2,2)
    plt.title(labels[side % 3] + " vs. " + labels[(side + 1) % 3] + ", UNCUT:")
    plt.plot(toPlotBadOld[side % 3], toPlotBadOld[(side + 1) % 3], 'bo')
    plt.plot(toPlotGood[side % 3], toPlotGood[(side + 1) % 3], 'ro')
    plt.xlabel(labels[side % 3])
    plt.ylabel(labels[(side + 1) % 3])
    plt.rcParams["figure.figsize"] = (12, 6)
    plt.show()
    
    
tosort = np.array([(np.sqrt((mainCam.field/np.max(mainCam.field[~np.isinf(mainCam.field)]))**2. + 
                           (mainCam.tfield/np.max(mainCam.tfield[~np.isinf(mainCam.tfield)]))**2. + 
                           (mainCam.ffield/np.max(mainCam.ffield[~np.isinf(mainCam.ffield)]))**2.)),
    (mainCam.UBrange[mainCam.bestIndex[0]]),                
    (mainCam.Trange[mainCam.bestIndex[1]])])
                           
                           
## plot top k against the rest of them
#        plt.plot(field[self.nindexnot], tfield[self.nindexnot], 'bo')
#        plt.plot(field[self.nindextop], tfield[self.nindextop], 'ro')
#        plt.xlabel("Avg. Cost (abs(mean - exp) + std)")
#        plt.ylabel("Time (s)")

#plt.imshow(nfield.transpose(), 
#           cmap="gnuplot_r", 
#           interpolation="nearest", 
#           norm=pltc.LogNorm(),
#           extent = [(UBrange[0]),# - (0.1 * abs(UBrange[0]))),
#                     (UBrange[-1]),# + (0.1 * abs(UBrange[-1]))),
#                     (Trange[0]),# - (0.1 * abs(Trange[0]))), 
#                     (Trange[-1])], # + (0.1 * abs(Trange[-1])))],
#           aspect = "40")
#plt.xlabel("Image Upperbound")
#plt.ylabel("Threshold Parameter")
#plt.colorbar()
#plt.show()

#c = MUSEcameraSystem.CameraSystemObject()
#c.calibrateTargetNew(_upperBoundStart = 4000, _upperBoundEnd = 15000,
#                     _threshStart = 0, _threshEnd = 150,
#                     expectedRatio = 1.737373,
#                     ubSize = 40, tSize = 80, sampleNum = 15)


#
#def find_peaks(a):
#  x = np.array(a)
#  max = np.max(x)
#  lenght = len(a)
#  ret = []
#  for i in range(lenght):
#      ispeak = True
#      if i-1 > 0:
#          ispeak &= (x[i] > 1.8 * x[i-1])
#      if i+1 < lenght:
#          ispeak &= (x[i] > 1.8 * x[i+1])
#
#      ispeak &= (x[i] > 0.05 * max)
#      if ispeak:
#          ret.append(i)
#  return ret
##
##
##
#### top k indicies
##k = 5
#  
#field = c.field
#tfield = c.tfield
#UBrange = c.UBrange
#Trange = c.Trange
#
#field[np.isnan(field)] = float('inf')
#tfield[np.isnan(field)] = float('inf')
#field[tfield < 0] = float('inf')
#tfield[tfield < 0] = float('inf')
#
#nfield = np.sqrt((field/np.max(field[~np.isinf(field)]))**2. + (tfield/np.max(tfield[~np.isinf(tfield)]))**2.)
#
#counts, bins = np.histogram(nfield[~np.isinf(nfield)].ravel(), bins=25)
#k = counts[find_peaks(counts)[0]]
#plt.hist(nfield[~np.isinf(nfield)].ravel(), bins=25)
#plt.show()
#nindextop = np.unravel_index(np.argpartition(nfield.ravel(), k)[:k], nfield.shape)
#nindexnot = np.unravel_index(np.argpartition(nfield.ravel(), k)[k:], nfield.shape)
#
#
### plot top k against the rest of them
#plt.plot(field[nindexnot], tfield[nindexnot], 'bo')
#plt.plot(field[nindextop], tfield[nindextop], 'ro')
#plt.xlabel("Avg. Cost (abs(mean - exp) + std)")
#plt.ylabel("Time (s)")
#plt.show()
#
### heatmap of visited parameters
#plt.imshow(nfield, cmap="gnuplot", interpolation="nearest", extent = [3000, 12000, 0, 150], aspect = "40")
#plt.xlabel("Image Upperbound")
#plt.ylabel("Threshold Parameter")
#plt.colorbar()
#plt.show()
#
#
#
#
### other stuff
#indicies = (field != float('inf')) & (~np.isnan(field) & (tfield > 0.0))
#
#fclean = field[indicies]
#tclean = tfield[indicies]
#
#fclean /= np.max(fclean)
#tclean /= np.max(tclean)
#
### creating field of euclidian distance vals
#
#nindex = np.unravel_index(np.nanargmin(nfield), np.shape(nfield))
#
#plt.plot(UBrange, np.broadcast_to(Trange, (len(UBrange), len(Trange))), 'bo')
#
#plt.imshow(nfield, cmap="brg", extent = [3000, 12000, 0, 150], aspect = "auto")
#plt.colorbar()
#plt.show()
#muse = MUSEcameraSystem.CameraSystemObject()

#
#if muse.isConnected():
#    muse.resetVariables()
#
#    try:
#        while muse.RunPrgrm:

#            muse.grab()
#            muse.process()
#            muse.addValues(muse.unprocessedPixelValues)
#            try: 
#                muse.removeCopies()
#                if muse.c == muse.circles:
#                    x, y, ma, MA, theta = muse.pixelPrime.transpose()
#                    lastCenter = [np.mean(x),np.mean(y)]
#                    m = (y[1]-y[0])/(x[1]-x[0])
#                    b = y[0] - m*x[0]
#                    x = [int(i) for i in x]
#                    y = [int(i) for i in y]
#
#                    for i in range(2):
#                        cv2.circle(muse.img,(int(x[i]),int(y[i])),1, 100)
#                    
#                    
#                        
#                        
#                    x0 = min(x[0],x[1]) - 100
#                    x1 = max(x[0],x[1]) + 100
#                    cv2.line(muse.img, (x0, int(x0*m + b)), (x1, int(x1*m + b)), (150,255,150), 2)
##                    
##                    print "\r {},{}".format(
##                            np.mean(muse.pixelPrime[0, 2:3])/np.mean(muse.pixelPrime[1, 2:3]), muse.thresh),
#            except IndexError:
#                pass
#            
#
#            cv2.imshow("img", muse.img)
#            
#            [muse.thresh, xdum, ydum] = muse.keyCheckRoutine(checkType = "target")
#            
#        cv2.destroyAllWindows()
#
#    except Exception:
#        print (traceback.format_exc())
#        cv2.destroyAllWindows()
#
#del muse




#Return the Rigol's ID string to tell us it's there
#print(oscilloscope.query('*IDN?'))

    #                    else: 
    #                        muse.update()
    #                        if muse.updated and (muse.sampleCount < muse.numberOfSamples):
    #                            muse.saveValues()
    #                        if muse.sampleCount >= muse.numberOfSamples: 
    #                            success = True
    #                            muse.RunPrgrm = False
    #                        print '\r       Samples Taken: {} / {}'.format(muse.sampleCount, muse.numberOfSamples),
          
    

