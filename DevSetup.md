### MUSE Camera System Development Environment Setup
Follows is a step-by-step list of dependencies necessary to successfully run the camera system code, at its current state. Contact luclepot@umich.edu with questions.

1.	Install [32-bit Python Anaconda for Python 2.7](https://www.anaconda.com/download/) on your computer. Make sure it runs correctly. We use this as the package manager//dev env setup (comes with Spyder etc.), however if you prefer something else and know how to install it feel free. In any case, 32 bit python 2.7 is a must.
If you want to install for multiple users, make sure to begin doing so here. Assume from here on out all commands are done as admin if so.

2. Open an anaconda (or equivalent) terminal and install opencv. This essentially means going into an admin prompt and hitting ```$ pip install opencv-python```. It will automatically get you the correct version.

3. Resolve dependencies, etc. There will invariably be packages to be installed and opencv might have a hard time installing right away. Simply look through the output and identify what else you should install first before trying to run an opencv installation.  

4. Test: This can be done by opening a terminal in the same directory as a standard image type (JPG, TIFF, etc.) and running the following few commands: 
```
    $ python  
    >>> import cv2  
    >>> import matplotlib.pyplot as plt
    >>> cv2.__version__  
    >>> img = plt.imread('<exact_desired_test_image_name_with_filetype>', 1)  
    >>> grey = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)  
    >>> cv2.imshow("test", img); cv2.waitKey(0)
    >>> cv2.imshow("grey test", grey); cv2.waitKey(0)
```
   This should show your test image along with a greyscale version of it quite nicely. This provides a good verification that OpenCV and its internal libraries are working at least somewhat correctly.

5. Installations for connecting to power supply for operating LED ring light:
The instructions here are for connecting to an Agilent E3632A power supply via the GPIB-USB-HS interface. 

a) Install pyvisa. Similar to above, in a terminal hit ```$ pip install -U pyvisa.py```. Again resolve the dependencies as necessary and test it by importing into python.

b) Install NI-488.2 driver for GPIB-USB-HS interface (http://www.ni.com/en-us/support/downloads/drivers/download.ni-488-2.html)

c) Follow installation instructions on website. At the end of installation, restart the PC. 

d) Check the status indicator on the GPIB cable. Stable amber (for USB 2.0) or green (for USB 1.0) color shows that connection is established. 

e) To confirm, open Anaconda Prompt and give the following commands
>>> python
>>> import visa
>>> rm = visa.ResourceManager()
>>> res = rm.list_resources()
>>> print(res)

If the installation went fine, then this will print available instrument addresses to establish interface:
(u'ASRL1::INSTR', u'ASRL3::INSTR', u'GPIB::1::INSTR')

Additional resources: GPIB user guide

6. Initialize and clone the git repository to a spot you want. ```git clone https://bitbucket.org/luclepot/muse_camera_system.git```

7. Set up the drivers for the uEye camera. This means installing the [current version of the uEye device driver](https://en.ids-imaging.com/download-ueye-win32.html).

You should now be all set to begin development. For recompilation instructions, check the recompilation file in this repo.
