"""
Function:
    UM MUSE Group Target Position Monitor, power control
    (A file for controlling an Agilent Power Supply)
"""
__author__ = [
        "Sabrina Corsetti, University of Michigan Department of Physics",
        "Luc Le Pottier, University of Michigan Department of Physics"
        ]
__email__ = [
        "sabcorse@umich.edu",
        "luclepot@umich.edu"
        ]



import visa
import traceback

#Mdimond 2019 for bk precision PSU
import serial
import time


"""
NOTE: Module may need modification based on power supply type used. If this is the
case, please make modifications to the following:
    - Install appropriate driver. For current power supply, driver can
      be found at http://www.ni.com/en-us/support/downloads/drivers/download.ni-488-2.html
    - Update voltage writing command, found in the setVolt() function. Specifically,
      update the line reading 'self.inst.write(...)' to match the correct voltage
      update command for whatever power supply is being used.

Note: Commands for writing and reading values are as follows:

inst.write("VOLT XX")
inst.query("CURR?")
inst.query("*IDN?") <-- A good communication test

"""
"""
class PowerSupply():
    name = None
    connected = False
    raiseEx = False
    listResourcesOnStartup = True
    voltage = 0

    def __init__(self, name = None, raiseEx = False, listRecs = True):
        '''
        Initializes an instrument.
        If name included as a parameter, automatically opens communication.
        If not, lists resource names.
            If this is the case, choose the name of the resource you want and
            call connect(name).
        '''
        self.rm = visa.ResourceManager()
        self.listResourcesOnStartup = listRecs
        self.raiseEx = raiseEx

        if name is None:
            if self.listResourcesOnStartup:
                print("Select a resource to open (use member function connect(string name)): ")
                print (self.rm.list_resources())

        else:
            self.connect(name)

    def on(self):
        ## change this command as necessary for future power supplies
        self.inst.write("OUTP ON")

    def off(self):
        ## change this command as necessary for future power supplies
        self.inst.write("OUTP OFF")

    def setVolt(self, position):
        '''
        Sets voltage
        '''
        # voltage cap for powersupply
        if position <= 23.0:
            self.inst.write("VOLT " + str(position))
            self.voltage = position
        else:
            self.inst.write("VOLT 23.0")

    def getResourceTuple(self):
        return self.rm.list_resources()

    def connect(self, name):
        '''
        Opens communication with an initially unnamed instrument.
        Should print "Successfully opened" if successful.
        '''
        if self.connected:
            self.inst.close()
            self.connected = False

        if isinstance(name, (float, int, long)):
            recs = self.getResourceTuple()
            if name <= len(recs):
                name = recs[name - 1]
        try:
            print("Opening " + name)
            self.inst = self.rm.open_resource(name)
            self.name = name
            self.connected = True
            print("Successfully opened")
            self.setVolt(0)
            return
        except:
            print("Unable to connect to resource " + name)
            print(traceback.format_exc())
        if self.raiseEx:
            raise PowerSupplyException

    def __iadd__(self, other):
        self.setVolt(float(self.voltage) + float(other))
        return self

    def __isub__(self, other):
        self.setVolt(float(self.voltage) - float(other))
        return self

    ## weird but helpful: use '+' operator for assignment of voltage. I know, bad, but

    ## easy for debugging.
    def __add__(self, other):
        self.setVolt(float(other))
        return self

    def __sub__(self, other):
        self.setVolt(-float(other))
        return self

    def __str__(self):
        return ("Current Settings: " +
                str(float(self.inst.query("MEAS:VOLT:DC?"))) + "V, " +
                str(float(self.inst.query("MEAS:CURR:DC?"))) + "A")

    def close(self):
        '''
        Ends communication with a connected instrument.
        Should print "Successfully closed" if successful.
        '''
        if(self.connected):
            self.off()
            self.setVolt(0)
            self.inst.close()
        self.connected = False
        print("Successfully closed")

    def __del__(self):
        self.close()
        
"""
#new PSU class for serial power supply, used for the new BK 1687B PSU.
class PowerSupply():
    
    connected = False
    voltage = 0
    #intitializes the serial connection the the PSU in com4 based on
    #parameters described in the PSU user manual
    
    def __init__(self, name = None, raiseEx = False, listRecs = True):
        self.connect(name)
        
    
    def on(self):
        self.ser.write("SOUT0\r".encode())
        self.ser.flush()
        #in order to let the LED's stabilize once we turn that bad boy back on DEBUGG TURN BACK ON
        time.sleep(1)
        
    def off(self):
        self.ser.write("SOUT1\r".encode())
        self.ser.flush()
        
    def getResourceTuple(self):
        return self.rm.list_resources()
    
    def setVolt(self, position):
        #same as above, limiting voltages
        if position < 10.0:
            position = 10.0
        if position <= 23.0:
            #doin a bit of bad coding to remove the . from position
            self.voltage = position
            position = round(position, 1)
            position = str(position)
            position = position.replace(".", "")
            while len(position) < 3:
                position = position + "0"
            self.ser.write(("VOLT" + position + "\r").encode())
            self.ser.flush()
        else:
            self.ser.write("VOLT230\r".encode())
            
    def connect(self, name):
        self.ser = serial.Serial()
        self.ser.baudrate = 9600
        self.ser.port = 'COM4'
        self.ser.timeout = 5
        self.ser.open()
        if (self.ser.is_open):
            self.connected = True
        self.setVolt(000)
            
    def close(self):
        if self.connected:
            self.setVolt(000)
            self.ser.close()
            self.connected = False
        print("Successfully closed")
    
    def __del__(self):
        self.close()

  
class PowerSupplyException(Exception):
    def __init___(self,eargs):
        Exception.__init__(self,"PowerSupplyException raised with args {0}".format(eargs))
        self.eargs = eargs
