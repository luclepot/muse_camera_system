"""
Function:
    UM MUSE Group Target Position Monitor, master class (ties together all helper classes)
"""
__author__ = "Luc Le Pottier, University of Michigan Department of Physics"
__email__ = "luclepot@umich.edu"

import cv2
import ctypes
import numpy as np
from MUSEcamera import Cam
from MUSEimage import Img
from MUSEdata import Data
from MUSEkeys import Keys
from MUSEDROsystem import DROSystemObject
from MUSEpowerControl import PowerSupply, PowerSupplyException
from MUSEclose import ExitCameraException, CloseCamera
from colorama import Fore, Style, Back
from time import time, sleep
import matplotlib.pyplot as plt
import traceback
import os
from datetime import datetime
#mtdimon debug
import csv

class CameraSystemObject(Data, Img, Cam, Keys, CloseCamera):
    """
    An object containing all variables, data, parameters, functions, and subobjects required to sucessfully manipulate
    the muse camera system.
    Inherets all functions and data from the Data, Img, Cam, Keys, and CloseCamera classes. Note that none of
    these can be used as stand alone classes; they are designed really to make the organization of this system
    better (i.e. files less than 5k lines of code)
    """

    ## view types
    VIEW_RAW = 11
    VIEW_8_BIT = 12
    VIEW_PROCESSED = 13
    MAX_MEASUREMENT_COUNT = 20000

    def __init__(self, powerSupplyName = None, averageRange=1,camID=0, camType="EO",
     bits=16, maxCircleVariationForRecognition=0.05, maxPercentSimilarityOfFoundCircles = 5,
      beta=0.28, alpha=0.0314, upperBound=6025, lowerBound=71.9, PixelMeasurements=5,
       MetricMeasurements=3, NumberOfSamples=50, AverageRange=1, CirclesInTarget=1,
        MaxCircles=5, saveMetric=True, dataPath = "GUIfiles", maxSampleTime = 0.5):
        """
        Initializes camera system. Loads saved parameters and sets parameters,
        sets up camera and power supply, etc.
        """
        self.PATH = dataPath
        self.saveMetric = saveMetric
        try:
            ## attempt to load parameters
            if saveMetric:
                parameters = np.load(self.PATH + "\{}RingTarget{}.npz".format(
                        CirclesInTarget*2, camType))
                self.parameters = parameters
                self.r0 = parameters['rWorld']
                # self.dro.TakeDROReading()
                #self.d0 = np.mean(self.dro.ReadHeightData(self.dro.measurement_count - 1)) + 368.85
                self.d0 = parameters['distance'].astype(float)
                self.rPixel0 = parameters['rPixel']
                #what is F
                self.F = [(self.rPixel0[i]*self.d0)/self.r0[i] for i in
                          range(2*CirclesInTarget)]

                ## if camParams doesn't exist, init to default values
                try:
                    self.camParams = parameters['camParams']
                    self.targetCalibrated = True
                except:
                    print traceback.format_exc()
                    self.targetCalibrated = False
                    self.camParams = [100, 10000, 0]

                ## likewise with voltageParams
                try:
                    self.voltageParams = self.parameters['voltageParams']
                    if self.voltageParams[0] == -1 and self.voltageParams[1] == -1:
                        self.voltageCalibrated = False
                    else:
                        self.voltageCalibrated = True
                except:
                    print traceback.format_exc()
                    self.voltageParams = [-1, -1]
                    self.voltageCalibrated = False

        ## except case where NO target parameters exist. Default values.
        except IOError:
            print("Result: Unable to load target parameters")
            self.camParams = [100, 10000, 0]
            self.voltageParams = [-1, -1]
            self.targetCalibrated = False
            self.voltageCalibrated = False

        ## save generic inputs as members
        self.maxSampleTime = maxSampleTime
        self.maxPercentSimilarityOfFoundCircles = maxPercentSimilarityOfFoundCircles
        self.pixelMeasurements = PixelMeasurements
        self.metricMeasurements = MetricMeasurements
        self.numberOfSamples = NumberOfSamples
        self.averageRange = AverageRange
        self.circles = 2*CirclesInTarget
        self.maxCircles = MaxCircles
            
        ## generate all the possible necessary vectors as zero vectors
        self.sampleTimes = np.zeros(self.numberOfSamples)

        self.metricSamples = np.zeros(
                (self.numberOfSamples, self.circles, self.metricMeasurements))
        self.metric = np.zeros(
                (self.averageRange, self.circles, self.metricMeasurements))
        self.rawMetric = np.zeros(
                (self.averageRange, self.circles, self.metricMeasurements))
        self.metricPrime = np.zeros(
                (self.circles, self.metricMeasurements))
        self.metricOversized = np.zeros(
                (self.maxCircles, self.metricMeasurements))

        self.pixel = np.zeros(
                (self.averageRange, self.circles, self.pixelMeasurements))
        self.pixelPrime = np.zeros(
                (self.circles, self.pixelMeasurements))
        self.pixelOversized = np.zeros(
                (self.maxCircles, self.pixelMeasurements))

        self.precisionMeasurementCount = 0
        self.precisionMeasurements = np.zeros((self.MAX_MEASUREMENT_COUNT, 2, self.circles,
                                               self.metricMeasurements))
        self.precisionMeasurementTimes = np.zeros(self.MAX_MEASUREMENT_COUNT)

        self.pixelsPerMM = [0, 0]

        self.setSize = 2
        self.sampleCount = 0
        self.c = 0
        self.standard = np.zeros(
                (self.numberOfSamples, self.metricMeasurements))

        self.mCVFR = maxCircleVariationForRecognition
        self.camType = camType

        # include full path or copy dll into same folder as .py script
        ## VERY important that this line runs.
        self.uEyeDll = ctypes.cdll.LoadLibrary(self.PATH + "/uEye_api.dll")
        self.camID = camID
        self.bits = bits
        self.bitnum = 65535.0
        self.imgtype = np.uint16
        self.beta = beta
        self.alpha = alpha
        self.thresh = self.camParams[0]
        self.UB = self.camParams[1]
        self.LB = self.camParams[2]
        self.RunPrgrm = True
        self.LEDvolts = 21.0

        ## sets up the keypress changes with the appropriate sensitivity. See MUSEkeys Keys class.
        self.setupKeys([5, 500, 10, 18.0], [self.thresh, self.UB, self.LB, self.LEDvolts])

        self.zero = [0, 0, 0]
        self.updated = False
        self.loadCachedData()
        self.spc = "       "

        self.offset = np.zeros(2)

        self.powerSupply = PowerSupply(raiseEx=True, listRecs = False)
        self.powered = False

        ## attempt to setup the power supply.
        if powerSupplyName is not None:
            try:
                self.powerSupply.connect(powerSupplyName)
                self.powered = True
            except PowerSupplyException:
                self.powered = False

        if not os.path.exists(self.PATH):
            os.makedirs(self.PATH)
        else:
            self.loadCalibrationParameters()
        ## create a DRO object
        
        ## connect camera
        self.connect()

    def checkImg(self):
        """
        outdated old helper class which histograms a camera image.
        nice sometimes for debugging.
        """
        self.grab()
        sleep(1)
        self.grab()
        plt.hist(self.img16.ravel(), bins=25)
        cv2.imshow("img", self.img16)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    def loadCalibrationParameters(self):
        """
        as one would expect, loads camera calibration pattern if one exists.
        Prints an error if not.
        """
        try:
            self.parameters = np.load(
                    self.PATH + "/{}CamParameters.npz".format(self.camType))
            self.x, self.y, self.w, self.h = self.parameters['a']
            self.B = self.parameters['b']
            self.C = self.parameters['c']
            self.D = self.parameters['d']
            self.cameraCalibrated = True
        except IOError:
            print("Result: Unable to load camera parameters")
            self.cameraCalibrated = False

    def saveCachedData(self, path="measurements.npz"):
        """
        saves the measurements to the specified path. This can be current or past measurements
        """
        np.savez(self.PATH + "/" + path, pmc=self.precisionMeasurementCount, pm=self.precisionMeasurements, pmt=self.precisionMeasurementTimes)

    def loadCachedData(self, path="measurements.npz"):
        """
        loads (attempts to load, more) the measurmeents from the specified path.
        """
        fullPath = self.PATH + "/" + path
        try:
            pmtemp = np.load(fullPath)
            self.precisionMeasurementCount = pmtemp["pmc"]
            self.precisionMeasurements = pmtemp["pm"]
            self.precisionMeasurementTimes = pmtemp["pmt"]
            self.updated = True     
        except IOError:
            print "no historical measurements loaded."
            pass

    def resetVariables(self):
        """
        wipes the long-term measurement variables
        """

        # reset metric arrays for fill
        self.metricSamples = np.zeros(
                (self.numberOfSamples, self.averageRange,
                 self.circles, self.metricMeasurements))
        self.metric = np.zeros(
                (self.averageRange, self.circles, self.metricMeasurements))
        self.metricPrime = np.zeros((self.circles, self.metricMeasurements))

        # reset pixel arrays for fill
        self.pixel = np.zeros(
                (self.averageRange, self.circles, self.pixelMeasurements))
        self.pixelPrime = np.zeros((self.circles, self.pixelMeasurements))
        self.pixelOversized = np.zeros(
                (self.maxCircles, self.pixelMeasurements))
        self.sampleCount = 0
        self.RunPrgrm = True
        self.key = -1
        self.cnt = 0

    def meanAxisRatio(self):
        """
        target calibration helper. Currently implemented as returning the mean axis ratio of the
        two axes, by averaging the MA/ma for each ellipse found and taking the ratio of the larger
        to the smaller.
        (Note that this works in all but the worst user cases bc the user is instructed to position
        the target perpendicular to the focal plane of the camera and thus calibration should not
        witness any kind of ellipse)
        """
        return np.mean(self.pixelPrime[1, 2:4])/np.mean(self.pixelPrime[0, 2:4])

    def updateTargetCalibration(self):
        """
        called after a successful target calibration. Updates target calibration parameters.
        """

        nfield = np.sqrt((self.field/np.max(self.field[~np.isinf(self.field)]))**2. +
                                   (self.tfield/np.max(self.tfield[~np.isinf(self.tfield)]))**2. +
                                   (self.ffield/np.max(self.ffield[~np.isinf(self.ffield)]))**2.)
        mindex = np.argmin(nfield[self.bestIndex])
        self.newThresh = self.Trange[self.bestIndex[1]][mindex]
        self.newUB = self.UBrange[self.bestIndex[0]][mindex]

        self.thresh = self.newThresh
        self.UB = self.newUB

        mean = self.pixelPrime
        variance = self.pixelPrime
        print("       saving data...")
        np.savez(self.PATH + '\{}RingTarget{}'.format(2, self.camType),
                 rWorld = self.userInput[0:2],
                 rPixel = self.listify(mean[:,3]),
                 pixelVariances = self.listify(variance[:,3]),
                 distance = self.userInput[2],
                 camParams = [self.thresh, self.UB, self.LB],
                 voltageParams = [-1, -1])

        self.parameters = np.load(self.PATH + "\{}RingTarget{}.npz".format(
        2, self.camType))

        self.r0 = self.parameters['rWorld']
        self.d0 = self.parameters['distance'].astype(float)
        #self.dro.TakeDROReading()
        #self.d0 = np.mean(self.dro.ReadHeightData(self.dro.measurement_count - 1)) + 368.85
        self.rPixel0 = self.parameters['rPixel']
        #debug
        self.F = [(self.rPixel0[i]*self.d0)/self.r0[i] for i in
                  range(2)]
        self.camParams = self.parameters['camParams']
        self.voltageParams = self.parameters['voltageParams']
        self.voltageCalibrated = False
        self.targetCalibrated = True

        self.setupKeys([5, 200, 10, 18.0], [self.thresh, self.UB, self.LB, self.LEDvolts])

        print "update successful"

    def voltageCalibratePass(self, voltage, vspan, n=5):
        """
        sub function of calibrateVoltage, runs a search pass (10 elts) over the range [voltage - vspan, voltage + vspan)
        and returns the sorted results
        """
        voltageRange = np.arange(voltage - vspan, voltage + vspan, (vspan/10.0))
        voltageParams = np.empty(np.shape(voltageRange))
        axisRatio = self.r0[1]/self.r0[0]
        for i, v in enumerate(voltageRange):
            print '\r       Testing: {} Volts in range [{}, {})'.format(v, voltageRange[0], voltageRange[-1]),

            cnt = 0
            self.powerSupply.on()
            self.powerSupply.setVolt(v)
            sleep(.5)
            pvec = -np.ones(n + 1)

            while cnt < n + 1:
                self.grab()
                self.process()
                cv2.imshow("EO Voltage Calibration", self.img)
                self.addValues(self.unprocessedPixelValues)
                [tdum, xdum, ydum, dum] = self.keyCheckRoutine()
                if self.removeCopies() and n > 0:
                    pvec[cnt] = abs(self.meanAxisRatio() - axisRatio)/axisRatio
                else:
                    pass
                cnt += 1

            self.powerSupply.off()
            #clean up the pvec array
            pvec[pvec < 0] = float('nan')
            pvec = pvec[1:len(pvec)]
            #voltageparams is n array of ratio differences
            voltageParams[i] = np.mean(pvec[~np.isnan(pvec)])

        print ""
        cv2.destroyAllWindows()
        #does this actually work? the voltages need to be associated with a ratio difference- are they?
        #debug
        return tuple((voltageParams[np.argsort(voltageParams)], voltageRange[np.argsort(voltageParams)]))

    def calibrateVoltage(self, distance, n = 5):
        """
        handles and plots voltage binary search. breaks after multiple counts and
        when finding a value lower than 2.5% different from real.
        """
        calibrated = False
        if not self.powerSupply.connected:
            print "Power Supply not connected correctly. Retry."
            return
        if self.isConnected():
            voltage = 16.0
            span = 7.0
            count = 0
            plt.rcParams["figure.figsize"] = (8, 8)
            while True:
                diffs, voltages = self.voltageCalibratePass(voltage, span, n=n)
                voltage = voltages[0]
                plt.plot(voltages[~np.isinf(diffs)], diffs[~np.isinf(diffs)], 'bo')
                plt.title("ITERATION {}".format(count + 1))
                plt.xlabel("Voltages")
                plt.ylabel("Ratio Difference")
                plt.show()
                span /= 2.0
                #this if statement breaks if we aren't able to find a fit within 
                #the range of the power supply
                if (abs(voltages[0] - voltages[-1]) < 0.001 and count > 1):
                    break
                if abs(diffs[0]) < .1 and count > 1:
                    calibrated = True
                    break
                count += 1


            if (calibrated):
                self.voltageCalibrated = True
                self.minimumVoltage = voltages[0]
                self.minimumDistance = distance
                self.updateVoltageCalibration()
                print "finished calibration"
            elif(~calibrated):
                print "Voltage not calibrated, check for alignment and full view of target"


    def updateVoltageCalibration(self):
        """
        Called after a successful voltage calibrationself. Updates target/voltage calibration parameters.
        """

        print("       saving data...")
        #self.dro.TakeDROReading()
        #self.d0 = np.mean(self.dro.ReadHeightData(self.dro.measurement_count - 1)) + 368.85
        np.savez(self.PATH + '\{}RingTarget{}'.format(2, self.camType),
                 rWorld = self.r0,
                 rPixel = self.rPixel0,
                 pixelVariances = [],
                 distance = self.d0,
                 camParams = [self.thresh, self.UB, self.LB],
                 voltageParams = [self.minimumVoltage, self.minimumDistance])

        self.parameters = np.load(self.PATH + "\{}RingTarget{}.npz".format(
        2, self.camType))

        self.r0 = self.parameters['rWorld']
        self.d0 = self.parameters['distance'].astype(float)
        self.rPixel0 = self.parameters['rPixel']
        #
        #self.d0 = np.mean(self.dro.ReadHeightData(self.dro.measurement_count - 1)) + 368.85
        self.F = [(self.rPixel0[i]*self.d0)/self.r0[i] for i in
                  range(2)]
        self.camParams = self.parameters['camParams']
        self.voltageParams = self.parameters['voltageParams']
        self.voltageCalibrated = True
        self.targetCalibrated = True

        self.setupKeys([5, 200, 10, 18.0], [self.thresh, self.UB, self.LB, self.LEDvolts])

        print "update successful"

    def getCalibrationData(self, _upperBoundStart = 4000, _upperBoundEnd = 15000, _threshStart = 0, _threshEnd = 150, ubSize = 10, tSize = 10, sampleNum = 5, expectedRatio = float(1.7/1.2), showCameraFeed = True ):
        """
        Grid search target calibration, data collection portion. Later this data will be passed to the appropriate MUSEdata function
        """
        print(Fore.CYAN +
              "--- Attempting to run calibrateTarget function...")
        ubSize = int(ubSize)
        tSize = int(tSize)
        sampleNum = int(sampleNum)
        try:
            if self.isConnected():
                self.resetVariables()

                if self.powerSupply.connected:
                    self.powerSupply.setVolt(23)

                # fields over which to search (black box requires brute force search, or complicated
                # other algorithms that I don't have time to research//implement).
                self.field = np.empty((ubSize, tSize))
                self.tfield = np.empty((ubSize, tSize))
                self.ffield = np.empty((ubSize, tSize))
                # upperbound range
                UBs = _upperBoundStart
                UBe = _upperBoundEnd
                self.UBrange = np.arange(UBs, UBe, (UBe-UBs)/ubSize)

                # threshold range
                Ts = _threshStart
                Te = _threshEnd
                self.Trange = np.arange(Ts, Te, (Te - Ts) / tSize)

                # empty vectors and counters to fill during loop
                ratios = np.empty(sampleNum)
                times = np.empty(sampleNum)
                fits = np.empty(sampleNum)
                iteration = 0

                # iterate over all possible options of thresh and upper bound
                for ubi in range(ubSize):
                    self.UB = self.UBrange[ubi]
                    for ti in range(tSize):
                        # reset variables and fill ratios with zeros
                        i = 0
                        ratios.fill(-1.0)
                        times.fill(float('inf'))
                        self.thresh = self.Trange[ti]

                        if self.powerSupply.connected:
                            self.powerSupply.on()
                        # main do-while loop (v pythonic implementation) through N samples
                        while i < sampleNum and self.RunPrgrm:
                            print "\r       it: {}/{}".format(iteration, sampleNum*tSize*ubSize),
                            t0 = time()
                            self.grab()
                            self.process()
                            self.addValues(self.unprocessedPixelValues)

                            # try and find circle, assign cost to it based on self.meanAxisRatio

                            if self.removeCopies():

                                times[i] = time() - t0
                                ratios[i] = self.meanAxisRatio()
                                fits[i] = np.max(abs(self.pixelPrime[:,3] - self.pixelPrime[:,2]) / self.pixelPrime[:, 3])
                            cv2.imshow("EO Camera Feed", self.img)
                            [tdum, xdum, ydum, dum] = self.keyCheckRoutine()
                            if ratios[i] < 0.0 or times[i] > self.maxSampleTime or fits[i] > 0.1:
                                iteration += (sampleNum - i)
                                break

                            i += 1
                            iteration += 1
                        if self.powerSupply.connected:
                            self.powerSupply.off()
                        print "\r       it: {}/{}".format(iteration, sampleNum*tSize*ubSize),

                        temp = np.mean(ratios)
                        if temp == -1:
                            self.field[ubi, ti] = float('inf')
                            self.tfield[ubi, ti] = float('inf')
                            self.ffield[ubi, ti] = float('inf')
                        else:
                            temp = np.mean(ratios[np.where(ratios != -1)])
                            self.field[ubi, ti] = abs(temp - expectedRatio) #+ np.std(ratios[np.where(ratios != -1)])
                            self.tfield[ubi, ti] = np.mean(times[np.where(ratios != -1)])
                            self.ffield[ubi, ti] = np.mean(fits[np.where(ratios != -1)])

                cv2.destroyAllWindows()
                if iteration < sampleNum*tSize*ubSize:
                    print "\nFull Sample Spectrum not taken. Aborting."
                    return False
                #indicies = (self.field != float('inf')) & (~np.isnan(self.field) & (self.tfield > 0.0))
                self.tfield[np.isnan(self.field)] = float('inf')
                self.field[np.isnan(self.field)] = float('inf')
                self.field[self.tfield < 0] = float('inf')
                self.tfield[self.tfield < 0] = float('inf')

                np.savez(self.PATH + "/" + "gridSearchData", tf = self.tfield, ff = self.ffield, f = self.field, ubr = self.UBrange, tr = self.Trange)

                return True
        except ExitCameraException:
            pass

    def calibrateCamera(self):
        """
        Calibrates the camera using a chessboard routine. Pretty outdated, but still in use under extreme circumstances. Would advise avoiding.
        """
        print(Fore.CYAN + "--- Attempting to run calibrateCamera function..." + Style.RESET_ALL)
        try:
            if self.isConnected():
                self.resetVariables()
                num = 8
                print("       Calibrate Camera using a square {0}x{0} chessboard pattern.".format(num))
                print("       Place chessboard at rest at various angles in front of ")
                print("       camera and press the spacebar to take a sample. It is  ")
                print("       advised to take 15-20 samples.")
                print("       When finished, press the 'esc' key, and be sure to look")
                print("       in output window to verify correct calibration.")

                scale = 5
                cornerCount = 0
                criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
                objP = np.zeros((num*num,3), np.float32)
                objP[:,:2] = scale*np.mgrid[0:num,0:num].T.reshape(-1,2)
                objectPoints = []
                imagePoints = []

                while self.RunPrgrm:
                    self.grab()
                    self.convertTo8Bit()
                    self.keyCheckRoutine(checkType = "camera")
                    if self.updated:
                        cv2.putText(self.img, "{} corners found".format(cornerCount),
                                    (10, 35), cv2.FONT_HERSHEY_SIMPLEX, 0.55,
                                    (self.bits/2,self.bits,self.bits), 2)

                        if self.key == 32:
                            ret, corners = cv2.findChessboardCorners(cv2.UMat(self.img),
                                                                     (num,num) )
                            if ret:
                                objectPoints.append(objP)
                                cv2.cornerSubPix(self.img,corners,(11,11),(-1,-1),criteria)
                                imagePoints.append(corners)
                                self.img = cv2.drawChessboardCorners(self.img,
                                                                     (num,num),
                                                                     corners,
                                                                     ret)
                                cornerCount += 1
                        cv2.imshow("EO Camera Feed", self.img)
                cv2.destroyAllWindows()
                if objectPoints and imagePoints and self.img is not None and cornerCount >= 10:
                    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objectPoints, imagePoints, self.img.shape[::-1],None,None)
                    h,  w = self.img.shape[:2]
                    cameraMatrix, roi = cv2.getOptimalNewCameraMatrix(mtx,dist,(w,h),0,(w,h))
                    np.savez(self.PATH + "\EOCamParameters.npz",a=roi,b=mtx,c=dist,d=cameraMatrix)
                    print(self.spc + "New camera parameters saved!")
                else:
                    print(self.spc + "Less than 10 samples taken, camera parameters unchanged.")

            print(Fore.CYAN + "--- Finished running calibrateCamera function." + Style.RESET_ALL)
        except ExitCameraException:
            pass

    def viewCameraFeed(self, viewType=VIEW_8_BIT, distance = None):
        '''
        Opens an opencv window showing the camera feed. Type of feed is based
        on the input variable, viewType.

        Options for viewType variable:
            self.VIEW_RAW: shows raw 16-bit footage
            self.VIEW_8_BIT: shows converted 8-bit footage
            self.VIEW_PROCESSED: shows greyscaled, threshed footage

        '''
        print(Fore.CYAN + "--- Attempting to view camera feed..." + Style.RESET_ALL)

        try:
            if self.isConnected():
                if distance == None:
                    #self.dro.TakeDROReading()
                    #distance = np.mean(self.dro.ReadHeightData(self.dro.measurement_count - 1)) + 368.85
                    distance = self.d0

                print("       Press \"esc\" to exit viewer and return to main program.")
                if self.powerSupply.connected:
                    self.powerSupply.on()
                    self.powerSupply.setVolt(self.getVoltage(distance))

                self.resetVariables()
                while self.RunPrgrm:
                    self.grab()
                    if viewType != self.VIEW_RAW:
                        self.convertTo8Bit()
                        self.undistortImage()

                        print "\r       Thresh, UB, LB: [{}, {}, {}]".format(self.thresh, self.UB, self.LB),

                        if viewType != self.VIEW_8_BIT:
                            self.makeGray()
                        cv2.imshow("EO Camera Feed", self.img)
                    else:
                        cv2.imshow("EO Camera Feed", self.img16)
                    [self.thresh, self.UB, self.LB, dum] = self.keyCheckRoutine(checkType = "target")


                cv2.destroyAllWindows()
                print("")
                if self.powerSupply.connected:
                    self.powerSupply.off()
        except ExitCameraException:
            cv2.destroyAllWindows()

        except KeyboardInterrupt:
            cv2.destroyAllWindows()

        print(Fore.CYAN + "--- Finished viewing camera feed\n\n" +
              Style.RESET_ALL)

    def precisionMeasurement(self, measurements=30, view=False, distance=None):
        """
        Initalizes camera and performs a measurement of 'measurements' frames
        of camera data. Closes and cleans up viz/object when finished, if needed.

        """
        print(Fore.CYAN +
              "--- Attempting to take a precision measurement...      "
              + Style.RESET_ALL)
        print(
              "       If more than 30 blank frames are taken in a row, program will\n"
              "       automatically abort the measurement without completing it. "
              )
        print("printing test")
        print(distance)
        try:
            if self.isConnected() and self.voltageCalibrated:
                self.numberOfSamples = measurements
                self.resetVariables()
                failedFrames = 0

                if distance == None:
                    #self.dro.TakeDROReading()
                    # distance = np.mean(self.dro.ReadHeightData(self.dro.measurement_count - 1)) + 368.85
                    distance = self.d0
                    #self.d0 = distance
                    #print(distance)
                if self.powerSupply.connected:
                    self.powerSupply.on()
                    print(distance)
                    self.powerSupply.setVolt(self.getVoltage(distance))
                try:
                    while self.RunPrgrm and (self.sampleCount <
                                             self.numberOfSamples) and (failedFrames < 30):
                        self.grab()
                        self.process()
                        self.addValues(self.unprocessedPixelValues)
                        #debug mtdimon march 2019 (note, x and y correspond to x and z but i am using the metrics
                        #from the fit ellipses functions)
                        '''
                        for i in self.unprocessedPixelValues:
                            print ("Center coordinate: (" + str(i[0]) +", " + str(i[1]) + ") \r")
                        '''
                        self.update(distance)
                        if not self.updated:
                            failedFrames += 1

                        if self.updated and (self.sampleCount <
                                             self.numberOfSamples):
                            failedFrames = 0
                            self.saveValues()

                        # self.histit()
                        if view:
                            # cv2.imshow("unprocessed", self.img16)
                            cv2.imshow("img", self.img)

                        [xdum, self.UB, self.LB, self.LEDvolts] = self.keyCheckRoutine()
                        self.cnt += 1
                    
                        print '\r       Samples Taken: {}'.format(self.sampleCount),
                    cv2.destroyAllWindows()

                except KeyboardInterrupt:
                    cv2.destroyAllWindows()
                if self.powerSupply.connected:
                    self.powerSupply.off()
            if self.numberOfSamples == self.sampleCount:
                print(Style.BRIGHT + Fore.GREEN +
                      "\n       Measurement success" + Style.RESET_ALL)
            else:
                print(Style.BRIGHT + Fore.RED +
                      "\n       MEASUREMENT ABORTED, NO DATA SAVED" +
                      Style.RESET_ALL)
            print(Fore.CYAN +
                  "--- Finished attempting precision measurement"
                  + Style.RESET_ALL)
        except ExitCameraException:
            pass

    def isConnected(self):
        """
        Checks whether the camera system is connected enough to take measurements. Boolean return value.
        """
        if self.connected and self.cameraCalibrated and self.targetCalibrated:
            return True
        else:
            print(Style.BRIGHT + Fore.RED + "       Unable to run function: ")
            if not self.connected:
                print("       - camera not connected")
            if not self.cameraCalibrated:
                print("       - camera is not calibrated or camera parameter file is missing")
            if not self.targetCalibrated:
                print("       - target is not calibrated or target parameter file is missing")
            return False

    def closeCameraSystem(self):
        self.saveCachedData()
        self.safeClose(exception="continue")

    """OUTDATED, IGNORE"""'''
    def calibrateTarget(self):
        """
        OLD calibration of target function. Now very outdated, use the routine of getCalibrationData shown in
        the GUImainConnected.py code.
        """
        print(Fore.CYAN +
              "--- Attempting to run calibrateTarget function...")
        try:
            if self.isConnected():
                self.resetVariables()
                print("       Calibrate target using a target of concentric circles")
                print("       with known radii.")
                print("       Place the target at the desired zero position from the")
                print("       camera, such that it is as close to perfectly")
                print("       perpendicular to the camera's focal plane axis as")
                print("       possible.")
                print("       IMPORTANT: ")
                print("       Adjust the light source brightness so that the inner to")
                print("       outer axis ratio is as close as possible to the ratio on")
                print("       the physical target.")
                print("")
                print("       When the target is properly positioned and the light is")
                print("       properly bright, press the spacebar to begin calibration.")
                print("       The program will then prompt the user to input target ")
                print("       parameters when finished taking samples.")
                print("")
                calibStart = False
                newInput = False
                success = False
                while self.RunPrgrm:
                    self.grab()
                    self.process()
                    self.addValues(self.unprocessedPixelValues)
                    if newInput:
                        print ""
                        newInput = False
                    if not calibStart:
                        try:
                            self.removeCopies()
                            if self.c == self.circles:
                                print "\r       Thresh, UP, LB: [{}, {}, {}]      Inner/outer axis ratio: {:.3f}".format(self.thresh, self.UB, self.LB, np.mean(self.pixelPrime[0, 2:3])/np.mean(self.pixelPrime[1, 2:3])),
                        except IndexError:
                            pass
                    else:
                        self.update()
                        if self.updated and (self.sampleCount < self.numberOfSamples):
                            self.saveValues()
                        if self.sampleCount >= self.numberOfSamples:
                            success = True
                            self.RunPrgrm = False
                        print '\r       Samples Taken: {} / {}'.format(self.sampleCount, self.numberOfSamples),

                    cv2.imshow("img", self.img)
                    if self.powerSupply.connected:
                        self.powerSupply.setVolt(self.LEDvolts)
                    [self.thresh, xdum, ydum, self.LEDvolts] = self.keyCheckRoutine(checkType = "target")

                    if self.key == 32:
                        calibStart = True
                        newInput = True

                print("")
                print("")

                if success:

                    radii = np.zeros(2)
                    print("       please enter radii of circles in order from largest to smallest, in mm:")
                    for i in range(np.size(radii)):
                        radii[i] = self.getInput(inputPrompt = "       radius {} = ".format(i+1), inputType = float(1.0))
                    print("")
                    print("        enter distance of camera from target for calibration, in mm:")
                    distance0 = self.getInput(inputPrompt = "       distance = ", inputType = float(1.0))

                    mean = np.mean(self.pixel,axis=0)
                    variance = np.var(self.pixel,axis=0)
                    print("       saving data...")

                    np.savez(self.PATH + '\{}RingTarget{}'.format(self.c, self.camType),
                             rWorld = radii,
                             rPixel = self.listify(mean[:,3]),
                             pixelVariances = self.listify(variance[:,3]),
                             distance = distance0,
                             camParams = [self.thresh, self.UB, self.LB])

                    print("       finished")
                elif self.numberOfSamples > self.sampleCount:
                    print("       Not enough samples taken, parameters not saved.")
                    print("       please try again.")
                else:
                    print("       Unknown error, parameters not saved.")
                    print("       Please try again.")



        except ExitCameraException:
            pass
        cv2.destroyAllWindows()
        print("--- Finished running calibrateTarget function.\n\n" + Style.RESET_ALL)'''
