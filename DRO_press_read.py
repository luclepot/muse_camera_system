# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import time
from pywinauto.application import Application
from pywinauto.controls.win32_controls import ButtonWrapper
from pandas import DataFrame, read_csv
import pandas as pd 


## start the process
##debug
##set the buttons that we are going to automate.
"""
open_file = utility['EL Series software']['Button 5']
curr_val = utility['EL Series software']['Button 6']
open_file.click()
"""
"""function to initialize variables used by pywinauto, later need to implement 
a check to make sure DRO is connectd properly, should also choose a filename-
having trouble getting pywinauto to detect the "save_as" dialogue. """
def StartAutoDRO():
    try:
        utility = Application(backend="uia").start(r"C:\Program Files (x86)\EL_Series_utility.exe")
    except:
        print("Unable to initialize DRO read")
    

def ReadHeightData(filename, row):
    data_sheet = pd.read_excel(filename)
    x_val = data_sheet['X'][row]
    y_val = data_sheet['Y'][row]
    z_val = data_sheet['Z'][row]
    
