"""
Function:
    UM MUSE Group Target Position Monitor, exit protocol subclass
"""
__author__ = "Luc Le Pottier, University of Michigan Department of Physics"
__email__ = "luclepot@umich.edu"

from colorama import Fore, Style
import colorama
colorama.init()

class ExitCameraException(Exception):
    """
    Custom derived exception class for caught Camera system exit exceptions.
    Functions exactly the same as a default Exception object.

    """
    pass

class CloseCamera():
    """
    Closes camera system safely. 
    FUTURE CAMERASYSTEM RELATED CLASSES MUST BE DERIVED FROM THIS IF YOU WANT
    TO USE BASIC FUNCTIONS SUCH AS "release()" OR "safeClose()"
    
    """
    
    def release(self):
        '''
        Releases EO camera and frees dynamic memory.

        '''
        self.uEyeDll.is_ExitCamera(self.cam)
        if(self.powerSupply.connected):
            self.powerSupply.close() 
            
        print(Fore.GREEN + "Safely exited camera system." + Style.RESET_ALL)

    def safeClose(self, exception=""):
        '''
        Safely closes the run object and resolves all subobjects;
        this includes camera and any open CV2 windows.

        Requires:
        ---------
        exception, optional.
            Error text if closing through error, blank if not.
            IF YOU DON'T WANT AN EXCEPTION: input "continue"

        '''
        if exception != "continue" and exception:
            print("")
            print("")
            print("")
            print(#Style.BRIGHT + Fore.RED + 
                    "///  EXCEPTION CAUGHT  ///")
            print("///  CLOSING PROGRAM   ///"
                  #+ Fore.RESET
                  )
            print("")
            print("Full error message: ")
            #print(Fore.YELLOW)
            print(exception)
            #print(Fore.RESET_ALL)
        else:
            print(#Style.BRIGHT + Fore.BLUE + 
                  "Ending camera system processes..." 
                  #+ Fore.RESET
                  )
        self.release()
        print("")
        if (exception == "continue"):
            return
        raise ExitCameraException
