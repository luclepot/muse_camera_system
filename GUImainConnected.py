# The code for changing pages was derived from: http://stackoverflow.com/questions/7546050/switch-between-two-frames-in-tkinter
# License: http://creativecommons.org/licenses/by-sa/3.0/

"""
Function:
    UM MUSE Group Target Position Monitor GUI
"""
__author__ = "Luc Le Pottier, University of Michigan Department of Physics"
__email__ = "luclepot@umich.edu"


import matplotlib
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure

from time import strftime, sleep
import Tkinter as tk
import ttk
import threading
import numpy as np
import tkMessageBox
import MUSEcameraSystem
from MUSEDROsystem import DROSystemObject
from colorama import Fore, Style, Back
import colorama
from datetime import datetime
import pandas as pd
import os
import tkSimpleDialog
from traceback import format_exc

colorama.init()

LARGE_FONT= ("Verdana Bold", 12)


"""
helper GUI classes and functions, for input. Usually open windows
extrinsic to main GUI pages.
"""
class powerSelect(tkSimpleDialog.Dialog):
    """
    Power selection class. Opens a small window in which the correct power
    supply choices are avaliable in a selection box, from which the user can
    select an input choice.
    """
    def body(self, master):
        """  
        main body of box which pops up. Initialize physical objects
        """
        ## label for input, variable (Initialized empty)
        tk.Label(master, text="Supply Port:").grid(row=0)
        self.var = tk.StringVar(master)
        a = mainCam.powerSupply.getResourceTuple()
        self.var.set("")
        ## option menu object
        self.option = tk.OptionMenu(master, self.var, *a)
        self.option.grid(row=0, column=1)

    def ok(self, arg=False):
        """
        Overload of TKINTER simple dialog ok button
        """
        self.apply()
        self.cancel()

    def cancel(self, arg=False):
        """   
        Overload of TKINTER simple dialog cancel button
        """
        self.parent.focus_set()
        self.destroy()

    def apply(self):
        """
        Action definition for minigui
        """
        self.result = self.var.get()

class cutInput(tkSimpleDialog.Dialog):
    """
    Cut parameter input class. Facilitates cutting of data by getting the
    user input and applying it using the mainCam global variable.
    """

    def body(self, master):
        """
        input box and object definition area
        """
        tk.Label(master, font = "ansifixed", anchor="w", text=
   """
The red points on the graphs printed to standard output represent
the computer's choice for the best calibration points examined.

Review these graphs: they will probably look poor. To refine them,
continually make cuts with the intent of isolating the minimal values
of both real/witnessed axis ratio differential, ellipse axis ratio
difference (an approximation of eccentricity), and sample time.

The cut graphs will be shown zoomed on the left, and the uncut graphs
on the right. When you're finished, press "esc". To cancel, press the
"cancel" button.
To make an additional cut, input the desired cut bounds and press "ok";
this will limit the given field to a range of [cut_low, cut_high].
A blank input field will not make a bound cut on that range; input only
what you wish to change.
""").grid(row=0, column = 0, columnspan = 3)

        ## cut fields (*l => low, *h => h)
        self.fieldl = tk.StringVar(master)
        self.fieldh = tk.StringVar(master)
        self.ffieldl = tk.StringVar(master)
        self.ffieldh = tk.StringVar(master)
        self.tfieldl = tk.StringVar(master)
        self.tfieldh = tk.StringVar(master)

        ## label and entry sides
        side = "E"
        labelside = "W"

        ## label cut fields (1)
        tk.Label(master, font="ansifixed", text="Sample Time bounds: ").grid(row = 1, column=0, sticky=side)
        self.tfieldlEntry = tk.Entry(master, textvariable = self.tfieldl)
        self.tfieldhEntry = tk.Entry(master, textvariable = self.tfieldh)
        self.tfieldlEntry.grid(row=1, column=1, sticky=labelside)
        self.tfieldhEntry.grid(row=1, column=2, sticky=labelside)
        ## (2)
        tk.Label(master, font="ansifixed", text="Real/meas. axis ratio diff bounds: ").grid(row = 2, column=0, sticky=side)
        self.fieldlEntry = tk.Entry(master, textvariable = self.fieldl)
        self.fieldhEntry = tk.Entry(master, textvariable = self.fieldh)
        self.fieldlEntry.grid(row=2, column=1, sticky=labelside)
        self.fieldhEntry.grid(row=2, column=2, sticky=labelside)
        ## (3)
        tk.Label(master, font="ansifixed", text="Ellipse Axes diff bounds: ").grid(row = 3, column=0, sticky=side)
        self.ffieldlEntry = tk.Entry(master, textvariable = self.ffieldl)
        self.ffieldrEntry = tk.Entry(master, textvariable = self.ffieldh)
        self.ffieldlEntry.grid(row=3, column=1, sticky=labelside)
        self.ffieldrEntry.grid(row=3, column=2, sticky=labelside)

    def getatr(self, name):
        """
        helper function for converting stringVar to float.
        """
        temp = name.get()
        return None if len(temp) == 0 else float(temp)

    def apply(self):
        """
        Applies current result parameters and refines calibration
        """
        try:
            self.result = tuple(((self.getatr(self.fieldl), self.getatr(self.fieldh)),
                                 (self.getatr(self.ffieldl), self.getatr(self.ffieldh)),
                                 (self.getatr(self.tfieldl), self.getatr(self.tfieldh))))
        except ValueError:
            print "incorrect input format: bounds must be integer or float type."
            return

        ## run a pass of refining calibration info based on input parameters
        mainCam.refineCalibrationInfo(mainCam.field,
                                      mainCam.ffield,
                                      mainCam.tfield,
                                      mainCam.UBrange,
                                      mainCam.Trange,
                                      self.result)

    def ok(self, arg=False):
        """
        ok button overload (accepts Enter button as "ok")
        """
        self.apply()

    def cancel(self, arg=False):
        """
        cancel button ovverload; cancels if "cancel" pressed and succeeds if
        "esc" button (on keyboard) is pressed.
        """
        ## check for esc key press: (arg not default)
        if arg != False:
            mainCam.updateTargetCalibration()
            print "Successfully updated target calibration parameters!"
        else:
            print "Aborting."
        ## continue to parent destroy self
        self.parent.focus_set()
        self.destroy()

class voltageInput(tkSimpleDialog.Dialog):
    """
    input dialog for voltage calibration procedure
    """
    def body(self, master):
        """
        object definitions and layout for class
        """
        tk.Label(master, font = "ansifixed", anchor="w", text=
   """
To calibrate the voltage linear fit:
    - Place or move the target to the nearest distance from
      the camera.
    - Input this distance in the "distance" field below.
    - Ensure that the system is successfully controlling
      the power supply, and then click "ok" to begin. The
      success of the calibration will be written to standard
      output.
NOTE: when finished, check the resulting voltage using the
      "processed" camera feed view and ensure the value is
      actually good - there is always a chance for error in
      this procedure.
""").grid(row=0, column = 0, columnspan = 3)

        self.distance = tk.StringVar(master)

        side = "E"
        labelside = "W"

        ## label and distance entry
        tk.Label(master, font="ansifixed", text="Minimum Distance: ").grid(row = 1, column=0, sticky=side)
        self.distanceEntry = tk.Entry(master, textvariable = self.distance)
        self.distanceEntry.grid(row=1, column=1, sticky=labelside)

    def getatr(self, name):
        """
        helper class to handle string-float conversion of either numbers or empty;
        exception otherwise.
        """
        temp = name.get()
        return None if len(temp) == 0 else float(temp)

    def apply(self):
        """
        updates object result parameter on "ok"
        """
        try:
            self.result = self.getatr(self.distance)
        except ValueError:
            print "incorrect input format: bounds must be integer or float type."
            return

    def ok(self, arg=False):
        """
        updates result variable and self destructs
        """
        self.apply()
        self.parent.focus_set()
        self.destroy()

    def cancel(self, arg=False):
        """
        cancels calibration, self destructs
        """
        self.result = -1
        print "Canceled Voltage Calibration"
        self.parent.focus_set()
        self.destroy()

class sizeInput(tkSimpleDialog.Dialog):
    def body(self, master):
        """
        object definitions
        """
        tk.Label(master, font = "ansifixed", anchor="w", text=
   """
To calibrate a two concentric circle target:
    - Place the target at the furthest experimental distance
      from the camera, such that it is as close as possible to
      perfectly perpendicular to the camera's focal plane axis
    - Then, adjust the light source brightness to be 23V, or
      the otherwise determined maximum. This will be done
      automatically if you have already set up a power supply.
    - Next, fill out the parameter fields below. With the
      exception of the 'Inner' and 'Outer' radius parameters,
      these will be automatically filled out if left empty. Make
      sure to fill in values as precisely as you can for the radii.
      Same goes for the inital distance parameter.
    - Parameter Keys:
        - Inner/Outer Radii are metric measurements of the target
          radii, with as high precision as possible.
        - <param> Start, <param> end, and <param> N define a range
          over which to search the given parameter <param>. EG
          a range of [Start, End] with a step of (End - Start)/N
          will be searched for any given parameter.
        - N, the final input parameter, is the default
    - NOTES:
        It is advisable to play around with the PROCESSED
        camera view under the 'target view' page, to get an idea
        of a decent parameter range.
        Additionally, the calibrate search scales in
        O( (Thresh N) * (UpperBound N) ) time; if both of these
        are about ~40, the search will take around an hour. Keep
        this in mind when scaling up.
""").grid(row=0, column = 0, columnspan = 2)

        ## target radii variables
        self.rinner = tk.StringVar(master)
        self.router = tk.StringVar(master)

        self.dist = tk.StringVar(master)

        ## range variables for upper bound
        self.ubs = tk.StringVar(master)
        self.ube = tk.StringVar(master)
        self.ubn = tk.StringVar(master)

        ## range variables for threshold/image
        self.ts = tk.StringVar(master)
        self.te = tk.StringVar(master)
        self.tn = tk.StringVar(master)

        ## n input, total number of samples per image
        self.n = tk.StringVar(master)

        self.defaults = [["", ""], [5770.0,5870.0,5.0], [235.0,245.0,5.0], [5]]
        self.ubs.set("5770")
        self.ube.set("5870")
        self.ubn.set("5")
        self.ts.set("235")
        self.te.set("245")
        self.tn.set("5")
        self.n.set("5")
        side = "E"
        labelside = "W"
        ## rows of radius input values on GUI
        tk.Label(master, font="ansifixed", text="Inner Radius (Default 9.08 mm): ").grid(row = 1, column=0, sticky=side)
        self.rinnerEntry = tk.Entry(master, textvariable = self.rinner)
        self.rinnerEntry.grid(row=1, column=1, sticky=labelside)
        tk.Label(master, font="ansifixed", text="Outer Radius (Default 17.894 mm): ").grid(row = 2, column=0, sticky=side)
        self.routerEntry = tk.Entry(master, textvariable = self.router)
        self.routerEntry.grid(row=2, column=1, sticky=labelside)
        tk.Label(master, font="ansifixed", text="Farthest Distance: ").grid(row=3, column=0, sticky=side)
        self.distEntry = tk.Entry(master, textvariable = self.dist)
        self.distEntry.grid(row=3, column=1, sticky=labelside)
        ## rows of upper bound input values on GUI
        tk.Label(master, font="ansifixed", text="UpperBound Start: ").grid(row = 4, column=0, sticky=side)
        self.ubsEntry = tk.Entry(master, textvariable = self.ubs)
        self.ubsEntry.grid(row=4, column=1, sticky=labelside)
        tk.Label(master, font="ansifixed", text="UpperBound End: ").grid(row = 5, column=0, sticky=side)
        self.ubeEntry = tk.Entry(master, textvariable = self.ube)
        self.ubeEntry.grid(row=5, column=1, sticky=labelside)
        tk.Label(master, font="ansifixed", text="UpperBound N: ").grid(row = 6, column=0, sticky=side)
        self.ubnEntry = tk.Entry(master, textvariable = self.ubn)
        self.ubnEntry.grid(row=6, column=1, sticky=labelside)

        ## rows of threshold input values on GUI
        tk.Label(master, font="ansifixed", text="Thresh Start: ").grid(row = 7, column=0, sticky=side)
        self.tsEntry = tk.Entry(master, textvariable = self.ts)
        self.tsEntry.grid(row=7, column=1, sticky=labelside)
        tk.Label(master, font="ansifixed", text="Thresh End: ").grid(row = 8, column=0, sticky=side)
        self.teEntry = tk.Entry(master, text="R2:", textvariable = self.te)
        self.teEntry.grid(row=8, column=1, sticky=labelside)
        tk.Label(master, font="ansifixed", text="Thresh N: ").grid(row = 9, column=0, sticky=side)
        self.tnEntry = tk.Entry(master, text="R1:", textvariable = self.tn)
        self.tnEntry.grid(row=9, column=1, sticky=labelside)

        ## final row, n-value
        tk.Label(master, font="ansifixed", text="Measurement N: ").grid(row = 10, column=0, sticky=side)
        self.tnEntry = tk.Entry(master, text="R1:", textvariable = self.n)
        self.tnEntry.grid(row=10, column=1, sticky=labelside)

    def ok(self, arg=False):
        """
        ok operator Overload
        """
        self.apply()
        self.cancel()

    def cancel(self, arg=False):
        """
        self destructs
        """
        self.parent.focus_set()
        self.destroy()

    def apply(self):
        """
        updates result to be correct result format
        """
        self.result = [[self.rinner.get(), self.router.get(), self.dist.get()],
                [self.ubs.get(), self.ube.get(), self.ubn.get()],
                [self.ts.get(), self.te.get(), self.tn.get()],
                [self.n.get()]]

def checksure(task, function, *args):
    """
    helper function: wraps function//args with an ok/cancel message box
    (used for impactful choices so buttonsd can't be accidentally clicked)
    """
    if tkMessageBox.askokcancel("Yes", "Are you sure you want to perform task '{}'?".format(task)):
        function(*args)

def packbundle(*bundle):
    """
    helper function; packs a pundle of items into a grid with
    a pleasing set of padding settings
    """
    for item in bundle:
        item[0].grid(row=item[1],column=item[2],padx=30,pady=10)


"""
Main GUI page definitions, wrapping, and control.
"""
class cameraGUI(tk.Tk):
    """
    main parent class for Tkinter GUI as a whole.
    """
    def __init__(self, *args, **kwargs):
        """
        initializes tkinter gui, container size, frames, and initial frame to show.
        """
        tk.Tk.__init__(self, *args, **kwargs)

        ## give it a title and an icon
        tk.Tk.iconbitmap(self,"GUIfiles/target.ico")
        tk.Tk.wm_title(self, "Position Monitor Controller")

        ## give container settings
        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand = True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}

        ## add frames to frame dictionary from frame classes (defined later)
        for F in (StartPage, PageOne, PageTwo, PageThree):

            frame = F(container, self)

            self.frames[F] = frame

            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(StartPage)

    def show_frame(self, cont):
        """
        helper function, displays frames as a whole
        """
        frame = self.frames[cont]
        frame.tkraise()

class StartPage(tk.Frame):
    """
    tkinter start page class. Defines variables, buttons, and actions associated with the start page.
    """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self,parent)

        label = tk.Label(self, text="Home", font=LARGE_FONT)

        button = ttk.Button(self, text="Calibration",
                            command=lambda: controller.show_frame(PageOne))

        button2 = ttk.Button(self, text="Target View",
                            command=lambda: controller.show_frame(PageTwo))

        button3 = ttk.Button(self, text="Position View",
                            command=lambda: controller.show_frame(PageThree))
        button4 = ttk.Button(self, text="Reconnect Camera",
                               command = lambda: self.reconnectCamera())
        ''' ###COMMENT THIS BACK IN IF WE NEED TO USE A DIFFERENT POWER SUPPLY, AND UNCOMMENT BUTTON5 IN LINE 470
        button5 = ttk.Button(self, text="Connect Power Supply",
                               command = lambda: self.reconnectPower())
        '''
        saveButton = ttk.Button(self, text="Save Data to CSV",
                                command=lambda: self.saveToCSV())

        for thing in [label, button, button2, button3, button4,
                      #button5, #also comment this button back in per 461
                      saveButton]:
            thing.pack(padx=10,pady=20)
            
    def reconnectPower(self):
        """
        attempts to reconnect maincam power supply using input from the
        powerselect minigui defined earlier. multithreading implemented for this.
        """
        global mainCam
        global t

        a = powerSelect(app)
        answer = a.result

        if(answer):
            ## multithreading callback definition
            def callback():
                mainCam.powerSupply.connect(answer)
            ## check thread life, if not being used, then start work.
            if not t.isAlive():
                t = threading.Thread(target = callback)
                t.start()
            else:
                print "Thread in use; wait for current function to finish running."

    def reconnectCamera(self):
        """
        attempts to reconnect main camera to mainCam global object
        """
        global mainCam
        global t

        def callback():
            mainCam.saveCachedData()
            mainCam.connect()

        if not t.isAlive():
            t = threading.Thread(target     = callback)
            t.start()

    def saveToCSV(self):
        """
        saves current data logs to a CSV file
        """
        if not os.path.exists("data_logs"):
            os.makedirs("data_logs")
                
        #make a separate file to save individual frames    
        
        global mainCam
        global t

        def callback():
            dataFrame = pd.DataFrame(mainCam.getCSVArray())
            df = dataFrame.transpose()
            df.to_csv("data_logs/{}.csv".format(
                     datetime.now().strftime("%d-%m-%Y_%H.%M.%S")), header = ["time (s)", 
                                                                              "abs X (mm)", "+/- X (mm)",
                                                                              "abs Z (mm)", "+/- Z (mm)",
                                                                              "abs Y (mm)", "+/- Y (mm)"])

        if not t.isAlive():
            t = threading.Thread(target = callback)
            t.start()

        print("Data saved to log for time {}".format(
                datetime.now().strftime("%d-%m-%Y_%H.%M.%S")))

class PageOne(tk.Frame):
    """
    calibration page.
    """
    def __init__(self, parent, controller):
        global mainCam
        global t
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Calibration", font=LARGE_FONT)
        label.pack(pady=10,padx=10)

        button1 = ttk.Button(self, text="Back",
                            command=lambda: controller.show_frame(StartPage))
        button1.pack()

        button2 = ttk.Button(self, text="Calibrate Camera"
                             , command = lambda: self.calibrateCam())
                            #,command=lambda: controller.show_frame(PageTwo))
        button2.pack()

        button3 = ttk.Button(self, text="Calibrate Target",
                             command = lambda: self.calibrateTarg())
        button3.pack()

        button4 = ttk.Button(self, text="Calibrate Voltage",
                             command = lambda: self.calibrateVoltage())
        button4.pack()

    def calibrateTarg(self):
        """
        Very complex function. makes use of both sizeInput class and the cutInput
        classes defined above.
        multithreading NOT implemented here; confusing to implement with the
        two seperate input boxes. I could probably figure it out but ran out of time.
        """
        global mainCam
        global t

        ## init sizeinput parameter input
        d = sizeInput(app)


        self.continueCalibrate = False
        self.retarr = d.result

        ## cancel from sizeinput handling
        if self.retarr is None:
            print "Canceled or closed, run aborted."
            return

        ## default parameters, checking parameter validity, etc.
        exarr = [[-1, -1, -1], [5770.0,5870.0,5.0], [235.0,245.0,5.0], [5]]
        try:
            self.retarr = [[float(self.retarr[iarr][ielt]) if len(str(self.retarr[iarr][ielt])) > 0
                           else exarr[iarr][ielt] for ielt in range(len(self.retarr[iarr]))] for iarr in range(len(self.retarr))]
        except ValueError as V:
            err = "".join(V[0].split(': ')[1::])
            print "invalid input:", err, "is not an integer/float. Aborting."
            return
        radii = np.array(self.retarr[0])
        if len(radii[radii < 0]) > 0:
            print "invalid input: both radii values must be filled in with positive values. Aborting."
            return
        ubparams = self.retarr[1]
        tparams = self.retarr[2]
        n = self.retarr[3][0]
        mainCam.userInput = radii

        ## call mainCam calibration data function with parameters read in.
        if mainCam.getCalibrationData(_upperBoundStart = ubparams[0],
                                   _upperBoundEnd = ubparams[1],
                                   _threshStart = tparams[0],
                                   _threshEnd = tparams[1],
                                   expectedRatio = float(radii[1]/radii[0]),
                                   ubSize = ubparams[2],
                                   tSize = tparams[2],
                                   sampleNum = n,
                                   showCameraFeed = True):
            self.continueCalibrate = True

        if self.continueCalibrate:
            mainCam.refineCalibrationInfo(mainCam.field,
                              mainCam.ffield,
                              mainCam.tfield,
                              mainCam.UBrange,
                              mainCam.Trange)
            cutinput = cutInput(app)
        print("--- Finished running calibrateTarget function." + Style.RESET_ALL)

    def calibrateVoltage(self):
        """
        calls the voltageInput helper and sends voltageInput to mainCam calibrate voltage
        member function.
        input validation done within voltageInput, in case you were wondering.
        """
        global mainCam
        global t

        v = voltageInput(app)

        def callback():
            if v.result != -1:
                mainCam.calibrateVoltage(v.result)
        try:
            if not t.isAlive():
                t = threading.Thread(target=callback)
                t.start()
            else:
                print(mainCam.spc + "Please wait for the current function to finish running.")
        except AttributeError:
            t = threading.Thread(target=callback)
            t.start()

    def calibrateCam(self):
        """
        calls calibrate camera function from mainCam object. Probably outdated but
        only needs to really be done once, so don't worry about it
        Also, our camera is great.
        """
        global mainCam
        global t
        def callback():
            mainCam.calibrateCamera()
        try:
            if not t.isAlive():
                t = threading.Thread(target=callback)
                t.start()
            else:
                print(mainCam.spc + "Please wait for the current function to finish running.")
        except AttributeError:
            t = threading.Thread(target=callback)
            t.start()

class PageTwo(tk.Frame):
    """
    Target view page, defines and implements all target viewing that might need to be done.
    """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Target View", font=LARGE_FONT)
        label.pack(pady=10,padx=10)

        ## the only thing which varies about these buttons is their input enum value
        ## (RAW, 8_BIT, PROCESSED)
        button1 = ttk.Button(self, text="Back",
                            command=lambda: controller.show_frame(StartPage))
        #button2 = ttk.Button(self, text="Show Raw Camera Feed",
                            #command=lambda: self.showTarget(mainCam.VIEW_RAW))  
                            #PRoy, 26 apr 2019 commented this
        button3 = ttk.Button(self, text="Show 8Bit Camera Feed",
                            command=lambda: self.showTarget(mainCam.VIEW_8_BIT))
        button4 = ttk.Button(self, text="Show Processed Camera Feed",
                            command=lambda: self.showTarget(mainCam.VIEW_PROCESSED))
        button1.pack()
        
        #button2.pack()  #PRoy, 26 apr 2019 commented this
        button3.pack()
        button4.pack()
        self.dro = DROSystemObject("DRO_obj{}.xls".format(
                     datetime.now().strftime("%d-%m-%Y_%H.%M")))
        self.dro_file = "DRO_obj{}.xls".format(
                     datetime.now().strftime("%d-%m-%Y_%H.%M"))
        self.dro.TakeDROReading()
        ## optional curent distance varaible: will update voltage on power supply, if connected.
        self.distanceVar = tk.StringVar(self)
        tk.Label(self, text="Current Distance (optional):").pack()
        self.distanceEntry = ttk.Entry(self, textvariable = self.distanceVar).pack()

    def showTarget(self, viewType):
        global mainCam
        global t
        try:
            distancev = float(self.distanceVar.get())
        except:
            print format_exc()
            distancev = None
        def callback():
            self.dro.TakeDROReading()
            distancev = np.mean(self.dro.ReadHeightData(self.dro.measurement_count - 1)) + 367.65
            mainCam.viewCameraFeed(viewType=viewType, distance=distancev)
        try:
            if not t.isAlive():
                t = threading.Thread(target=callback)
                t.start()
            else:
                print(mainCam.spc + "Please wait for current function to finish before running another.")
        except AttributeError:
            t = threading.Thread(target=callback)
            t.start()

class PageThree(tk.Frame):
    """
    Big page with lots going on. Displays data if everything is calibrated.
    """
    AUTOSAMPLE = False
    def __init__(self, parent, controller):
        """
        Lots of commented out code here that might be helpful for examples, so I'll summarize here: shows plots, inputs, and all other relevant
        information.
        """

        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Position View", font=LARGE_FONT)
        self.grid()
        self.showErrorBars = False

        #Ask "Are you sure you would like to update...?"
        v = tk.StringVar()
        v.set("1")
       # plabel = tk.Label(self,text="# of times in a row to update: ")

        zerolabel = tk.Label(self, text = "\t\t\n")

        #self.datelabel = tk.Label(self,text="Date: \t{}".format())
        self.timelabel = tk.Label(self,text="{}, {}".format(strftime("%H:%M"), strftime("%m/%d/%Y")))
        #pinput = tk.Entry(self,text="frames", textvariable=v)
        self.xlabel = tk.Label(self,text="Δ x: \t{:.3f} \t+/- {:.3f} mm".format(0,0))
        self.zlabel = tk.Label(self,text="Δ z: \t{:.3f} \t+/- {:.3f} mm".format(0,0))
        self.ylabel = tk.Label(self,text="Δ y: \t{:.3f} \t+/- {:.3f} mm".format(0,0))
        self.dlabel = tk.Label(self,text="d: \t{:.3f} mm".format(0))
        homeButton = ttk.Button(self, text="Back to Home",
                            command=lambda: controller.show_frame(StartPage))
#        updateButton = ttk.Button(self,text="Update",
#                             command = lambda: self.getMeasurements( int(v.get()) ))
        errorButton = ttk.Button(self,text="Show/hide error bars", command = lambda : self.flipBool())
        self.f = Figure(figsize=(12,12), dpi=60)

        clearData = ttk.Button(self, text="Clear saved data",
                               command = lambda : checksure("clear saved data", self.clearData))
        self.f = Figure(figsize=(12,12), dpi=60)

        setZero = ttk.Button(self, text="Set zero position",
                             command = lambda : checksure("set zero position", self.setSystemZero))

        clearZero = ttk.Button(self, text="Clear zero position",
                               command = lambda : checksure("clear zero position", self.clearSystemZero))

        autoSampleStartButton = ttk.Button(self, text="Start Autosampling",
                                command = lambda : self.autoSampleStart())

        autoSampleEndButton = ttk.Button(self, text="Stop Autosampling",
                                   command = lambda : self.autoSampleEnd())
        
        self.xtime = self.f.add_subplot(311, xlabel="Time (hours)")
        self.ytime = self.f.add_subplot(312, xlabel="Time (hours)")
        self.ztime = self.f.add_subplot(313, xlabel="Time (hours)")
        #ADD DRO INPUT HERE WHEN READY
        #trying to use DRO measurements - hardcoding in the 367.65mm from the lens to the bullseye at home poisition
        self.dro = DROSystemObject("DRO_obj{}.xls".format(
                     datetime.now().strftime("%d-%m-%Y_%H.%M")))
        self.dro.TakeDROReading()
        self.dro_file = "DRO_obj{}.xls".format(
                     datetime.now().strftime("%d-%m-%Y_%H.%M"))
        self.distanceVar = tk.StringVar(self)
        self.distanceEntryLabel = tk.Label(self, text="Current Distance (optional):")
        self.distanceEntry = ttk.Entry(self, textvariable = self.distanceVar)

        packbundle([homeButton,0,0],[label,0,2],[self.timelabel,0,4],#[self.datelabel,0,3],
                   #[updateButton,2,0],
                   [zerolabel,1,1],
                   [errorButton,3,0],[clearData,2,0],
                   [self.xlabel,4,0],[self.zlabel,5,0],[self.ylabel,6,0], [self.dlabel, 7, 0],[zerolabel, 0, 4],
                   # [plabel,1,1],[pinput,1,2],
                   [setZero, 2, 4], [clearZero, 3, 4], 
                   #[self.distanceEntryLabel, 2,5],[self.distanceEntry, 3, 5],
                   [autoSampleStartButton, 2, 2], [autoSampleEndButton, 3, 2])

        self.canvas = FigureCanvasTkAgg(self.f, self)
        self.canvas.get_tk_widget().grid(row = 4, column = 1, columnspan = 3,rowspan = 4)

        self.updatePlots()

    def autoSampleStart(self, sampleTime = 2):
        """
        command which triggers multithreaded autosampling. Ended by the similarly named
        "autosampleend" button.
        """
        global mainCam

        ## try and get distance input from distanceVariable,
        ## for automatic voltage updating.
        try:
            distancev = float(self.distanceVar.get())
        except:
            distancev = None

        def callback():
            while self.AUTOSAMPLE:
                ## maybe put magnetic scale read-in and distancev overload
                ## here. ALSO you could do it inside of precision measurement
                ## Remember to change y-value of camera system to be the same as
                ## the magnetic scale value
                self.dro.TakeDROReading()
                dro_reading = -9999
                dro_reading = np.mean(self.dro.ReadHeightData(self.dro.measurement_count - 1))
                sleep(2)
                distancev = dro_reading + 367.65
                print(distancev)
                print("       Autosampling begun.")
                print("printing test: np.mean")
                print(dro_reading)
                mainCam.precisionMeasurement(view=False, distance=distancev)
                if mainCam.updated:
                    self.updatePlots()
                else:
                    self.autoSampleEnd()
                    break
                ## sleep for sampleTime amount of time. 5 seconds by default
                i = 0
                while i < sampleTime and self.AUTOSAMPLE:
                    sleep(0.1)
                    i += 0.1
                    
        self.AUTOSAMPLE_THREAD = threading.Thread(target = callback)
        self.AUTOSAMPLE = True
        self.AUTOSAMPLE_THREAD.start()

    def autoSampleEnd(self):
        """
        ends autosampling routing. Expect to reasonably see samples stop after the Current
        sample finishes running.
        """
        def callback():
            if not self.AUTOSAMPLE:
                return
            self.AUTOSAMPLE = False
            self.AUTOSAMPLE_THREAD.join()
            print("       Autosampling completed.")
        global t
        if not t.isAlive():
            t = threading.Thread(target = callback)
            t.start()

    def clearSystemZero(self):
        """
        clears the mainCam zero vector and updates plots. Multithreaded.
        """
        def callback():
            mainCam.zero = [0, 0, 0]
            self.updatePlots()
        global t
        if not t.isAlive():
            t = threading.Thread(target = callback)
            t.start()

    def setSystemZero(self):
        """
        attempts to set the mainCam zero vector (via new measurement) and updates plots. Multithreaded.
        """
        def callback():
            if not mainCam.updated:
                mainCam.precisionMeasurement(view=False)
            if mainCam.updated:
                mainCam.zero = mainCam.getPrecisionPlotArrays(zero=False)[:,mainCam.precisionMeasurementCount - 1][1:4]
                self.updatePlots()
                print("       Successfully updated zero position.")
            else:
                print("       Zero position could not be updated; please try again.")
        global t
        if not t.isAlive():
            t = threading.Thread(target = callback)
            t.start()

    def clearData(self):
        """
        clears cached data. Only do this if you really don't like the current data.
        Advised to save data to CSV first.
        """
        def callback():
            mainCam.resetData()
            self.updatePlots()

        global t
        if not t.isAlive():
            t = threading.Thread(target = callback)
            t.start()

    def flipBool(self):
        """
        turns off/on error bars and revisualizes plots
        """
        self.showErrorBars = not self.showErrorBars
        self.updatePlots()

    def getAxesLimits(self, values):
        """
        A simple function. Grabs plot axis limits from a set of values
        """
        if np.max(values) - np.min(values) > 0.2:
            return [1.1*np.min(values), np.max(values)*1.1]
        return [np.mean(values) - 0.1, np.mean(values) + 0.1]

    def updatePlots(self):
        """
        updates plots completely with information from maincam. Runs relatively quickly,
        so not multithreaded
        """

        mainCamPlot = mainCam.getPrecisionPlotArrays()
        pos = mainCam.getPrecisionPositionArrays()
        t = mainCamPlot[0,:]
        x = mainCamPlot[1,:]
        y = mainCamPlot[2,:]
        z = mainCamPlot[3,:]
#                    self.xtime.set_ylim(self.getAxesLimits(x))
#                    self.ytime.set_ylim(self.getAxesLimits(y))
#                    self.ztime.set_ylim(self.getAxesLimits(z))
        self.xtime.plot(t, x)
        self.ytime.plot(t, y)
        self.ztime.plot(t, z)
        #d = distance from home position
        d = np.mean(self.dro.ReadHeightData(self.dro.measurement_count - 1))
        dist = d + 367.65
        errs = mainCam.getErrors()
        #DEBUG
        #for err in errs:
          #  if err[0] > 0.2 or err[1] > 0.2:
                
        if self.showErrorBars and np.size(errs) > 0:
            self.xtime.errorbar(t, x, yerr=mainCam.getErrors()[:,0], fmt='o')
            self.ytime.errorbar(t, y, yerr=mainCam.getErrors()[:,1], fmt='o')
            self.ztime.errorbar(t, z, yerr=mainCam.getErrors()[:,2], fmt='o')

        self.canvas.draw()
        self.xtime.clear()
        self.ytime.clear()
        self.ztime.clear()
        #self.datelabel = tk.Label(self,text="Date: \t{}".format(strftime("%m/%d/%Y")))
        self.timelabel = tk.Label(self,text="{}, {}".format(strftime("%H:%M"), strftime("%m/%d/%Y")))

        self.xlabel = tk.Label(self,text="x: \t{:.3f} \t+/- {:.3f} mm".format(pos[0,0],pos[1,0]))
        self.zlabel = tk.Label(self,text="z: \t{:.3f} \t+/- {:.3f} mm".format(pos[0,1],pos[1,1]))
        self.ylabel = tk.Label(self,text="y: \t{:.3f} \t+/- {:.3f} mm".format(pos[0,2],pos[1,2]))
        self.dlabel = tk.Label(self,text="d: \t\t{:.3f} mm".format(dist))
        
        packbundle([self.xlabel,4,0],[self.zlabel,5,0],[self.ylabel,6,0], [self.dlabel,7,0],[self.timelabel,0,4] )#[self.datelabel,0,3],

    def getMeasurements(self, times):
        """
        measurement function with no view. Automatic
        and multithreaded
        """
        def callback():
            rejected = False
            if times is not None and not rejected:
                for i in range(times):
                    mainCam.precisionMeasurement(view=False)
                    self.updatePlots()

        global t
        if not t.isAlive():
            t = threading.Thread(target = callback)
            t.start()


"""
main function: initalizes, runs app mainloop, and closes threads //camera system when finished.
"""
if __name__ == "__main__":
    mainCam = MUSEcameraSystem.CameraSystemObject()
    t = threading.Thread()

    app = cameraGUI()
    app.mainloop()

    try:
        if(t.isAlive()):
            t.join()
    except Exception as e:
        print e

    mainCam.closeCameraSystem()
