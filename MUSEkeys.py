"""
Function:
    UM MUSE Group Target Position Monitor, keypress // input management subclass
"""
__author__ = "Luc Le Pottier, University of Michigan Department of Physics"
__email__ = "luclepot@umich.edu"

import numpy as np
import cv2
from MUSEclose import CloseCamera

class Keys(CloseCamera):
    def setupKeys(self, dCs, args):
        '''
        Sets key object for fast use later. REQUIRED before using
        methods such as linearParamChange.

        Parameters
        ----------
        dCs: list of numerical values, required
            list of amounts by which to change each variable when using
            linearParamChange
        args: list of numerical values, required
            list of variables to change.

        '''
        self.dCs = list(dCs)
        self.args = list(args)
        self.upKeys = [113, 119, 101, 114, 116, 121, 117, 105, 111, 112, 91]
        self.downKeys = [97, 115, 100, 102, 103, 104, 106, 107, 108, 59, 39]
        self.argRange = range(np.size(args))
        self.getKey()

    def getKey(self):
        '''
        Gets the key being pressed. MUST BE CALLED FOR USE OF
        "linearParamChange" or "breakCheck" or printParams; suggested
        use is calling "keyCheckRoutine" which does this naturally.

        Modifies
        -------
        "self.key" -> integer keycode of key being pressed

        '''
        self.key = cv2.waitKey(1) & 0xFF

    def linearParamChange(self):
        '''
        Assign each parameter in args to a key pair, starting with 'q'
        and 'a' and going horizontally across the keyboard to '[' and '\''.

        Modifies
        -------
        values of arguments in 'args'. NOTE: doesn't requrie initial
        input of args, because this is the ONLY modification instance.

        '''
        for n in self.argRange:
            if self.key == (self.upKeys[n]):
                self.args[n] += self.dCs[n]
            elif self.key == (self.downKeys[n]):
                self.args[n] -= self.dCs[n]
        return tuple(self.args)

    def breakCheck(self):
        '''
        Check if key is the escape key, and break if so.

        '''
        if self.key == 27:
            self.RunPrgrm = False

    def printParams(self):
        '''
        If spacebar (key 32) is pressed, print every parameter in the list and
        their current values.

        '''
        if self.key == 32:
            i = 1
            for param in self.args:
                print("param {}:   {}").format(i, param)
                i += 1
            print("")

    def keyCheckRoutine(self, checkType=0):
        '''
        Suggested routine for checking

        '''
        self.key = -1
        self.getKey()
        toReturn = self.linearParamChange()
        self.breakCheck()
        if not checkType:
            self.printParams()

        return toReturn