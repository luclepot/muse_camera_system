"""
Function:
    UM MUSE Group Target Position Monitor, Edmund Optics handling camera functions subclass
"""
__author__ = "Luc Le Pottier, University of Michigan Department of Physics"
__email__ = "luclepot@umich.edu"


import ctypes
import numpy as np
import traceback
from MUSEclose import CloseCamera
from colorama import Fore, Style, Back

class Cam(CloseCamera):
    def connect(self):
        '''
        Connect to Edmund Optics camera.
        MUST USE RELEASE WHEN FINISHED WITH CAMERA FOR PROPER RESTART

        Requires:
        ---------
        Nothing

        Modifies:
        ---------
        Several camera-related variables; most importantly,
        initializes self.img for later use.

        '''
        # insure camera is not already initalized
        try:
            self.uEyeDll.is_ExitCamera(self.cam)
        except AttributeError:
            pass

        # Try and catch entire camera init makes it easier to debug
        try:
            # Setting up C types
            self.bitint = 28
            self.dtype = np.uint16

            self.cam = ctypes.c_uint32(self.camID)
            self.hWnd = ctypes.c_voidp()
            self.msg = self.uEyeDll.is_InitCamera(ctypes.byref(self.cam),
                                                  self.hWnd)

            self.ErrChk = self.uEyeDll.is_EnableAutoExit(self.cam,
                                                         ctypes.c_uint(1))
            print ""
            print ""
            print ""
            # Check if camera is initialized, if UEYEDLL is acessible
            if self.ErrChk:
                print(Fore.RED + Style.BRIGHT + 
                      "Camera was not able to be initialized.\n" + 
                      Style.NORMAL + 
                      "   No camera plugged in, ueye error code {}.".format(
                        self.ErrChk) + Style.RESET_ALL )

                self.connected = False

            else:
                self.connected = True
                print(Fore.GREEN + Style.BRIGHT +
                      'Camera connection established successfully'
                      + Style.RESET_ALL)

                # Setup optimaal camera settings like color mode, sensor mode,
                # trigger mode
                self.IS_CM_SENSOR_RAW16 = ctypes.c_int(self.bitint)
                nRet = self.uEyeDll.is_SetColorMode(self.cam,
                                                    self.IS_CM_SENSOR_RAW16)
                self.IS_SET_TRIGGER_SOFTWARE = ctypes.c_uint(0x1000)
                nRet = self.uEyeDll.is_SetExternalTrigger(
                        self.cam, self.IS_SET_TRIGGER_SOFTWARE)

                # set fps and gain
                self.fps = 30
                newFps = 0
                nRet = self.uEyeDll.is_SetFrameRate(
                        self.cam, ctypes.c_double(self.fps),
                        ctypes.c_double(newFps))

                gain = 0
                nRet = self.uEyeDll.is_SetHardwareGain(
                        self.cam, ctypes.c_int(gain), ctypes.c_int(gain),
                        ctypes.c_int(gain), ctypes.c_int(gain))
                # kill our return value
                if ~nRet:
                    del nRet

                # allocate memory
                self.width_py = 1280
                self.height_py = 1024
                self.pixels_py = self.bits

                # convert python values into c++ integers
                self.width = ctypes.c_int(self.width_py)
                self.height = ctypes.c_int(self.height_py)
                self.bitspixel = ctypes.c_int(self.pixels_py)

                # create placeholder for image memory
                self.pcImgMem = ctypes.c_char_p()
                self.pid = ctypes.c_int()

                self.ErrChk = self.uEyeDll.is_AllocImageMem(
                        self.cam, self.width, self.height,  self.bitspixel,
                        ctypes.byref(self.pcImgMem), ctypes.byref(self.pid))
                if ~self.ErrChk:
                    pass
                else:
                    print(''' Memory allocation failed,
                          no camera with value {}'''.format(self.cam.value))

                # Get image data
                self.uEyeDll.is_SetImageMem(self.cam, self.pcImgMem, self.pid)
                self.img16 = np.ones((self.height_py, self.width_py),
                                     dtype=self.dtype)

        # Close program if any of this broke, give full exception
        # traceback like a good program
        except Exception:
            self.safeClose(exception=traceback.format_exc())

    def grab(self):
        '''
        Grab image from Edmund Optics camera.
        MUST USE RELEASE WHEN FINISHED WITH CAMERA FOR PROPER RESTART

        Requires:
        ---------
        Pre-initialization of EO camera

        Modifies:
        ---------
        self.img file

        '''

        try:            
            self.uEyeDll.is_CaptureVideo(self.cam, ctypes.c_int(0x0000))
            self.gotImage = not bool(self.uEyeDll.is_CopyImageMem(
                    self.cam, self.pcImgMem, self.pid,
                    self.img16.ctypes.data))        

        except Exception:
            self.safeClose(exception=traceback.format_exc())

